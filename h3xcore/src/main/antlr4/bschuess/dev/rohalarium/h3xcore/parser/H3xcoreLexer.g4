lexer grammar H3xcoreLexer;

tokens {
	INDENT,
	DEDENT,
	LISTINDENT
}

options {
	superClass = IndentDedentLexerBase;
}

fragment DIGIT: [0-9];
fragment SPACES: [ \t]+;

// plain string for plain keys or values
fragment STR_PLAIN_START_CH : [a-zA-Z_];
fragment STR_PLAIN_CH: [0-9a-zA-Z_-];

// plain old string without h3x components
fragment STR_REGULAR_DELIM: '\'';
fragment STR_REGULAR_CH: ~([\r\n\f] | '\'' | '\\') ; 

// string with h3x code replacement
//fragment STR_H3X_DELIM: '"';
//fragment STR_H3X_CODE_O: '{';
//fragment STR_H3X_CODE_C: '}';
//fragment STR_H3X_CH: ~( '"' |'{' | '}'|  '\\') ;
	

NEWLINE:
	(
		{atStartOfInput()}? SPACES
		| ( '\r'? '\n' | '\r' | '\f') SPACES? ('-' SPACES?)?
	) {onNewLine();};



// special symbols
O_PAR: '(';
C_PAR: ')';
O_BRA: '[';
C_BRA: ']';
V_LINE: '|';
SELECTOR: '$';
COLON: ':';
DOT: '.';
LIST_SEP: ',';

// multiplicative group
MOD: '%';
POW: '**'; // POW or map unwrap
MULT: '*'; // MULT or list unwrap
DIV: '/';
CEIL_DIV: '///';
FLOOR_DIV: '//';

// commutative group
ADD: '+';
SUB: '-';

// comparable operators
LESS: '<';
LESS_EQ: '<=';
GREATER_EQ: '>=';
GREATER: '>';

EQ: '==';
NEQ: '!=';

// boolean operators
NOT: 'not';
AND: 'and';
OR: 'or';

// reference
SELF: 'self';

// primitives
NUMBER: DIGIT+ (DOT DIGIT+)?;
BOOL: ('True' | 'true' | 'False' | 'false');
STR_REGULAR: STR_REGULAR_DELIM STR_REGULAR_CH* STR_REGULAR_DELIM;
STR_PLAIN: STR_PLAIN_START_CH STR_PLAIN_CH*; 




SKIP_: ( SPACES ) -> skip;

UNKNOWN_CHAR : . ;

