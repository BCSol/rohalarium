parser grammar H3xcoreParser;

options {
	tokenVocab = H3xcoreLexer;
	language = Java;
}

fileInput:
	NEWLINE* (documents+=document NEWLINE*)* EOF;

document: 
	key COLON
		NEWLINE INDENT mapEntry+
		DEDENT;

mapEntry:
	  key COLON expr NEWLINE
	| key COLON NEWLINE INDENT
		mapEntry+
	  DEDENT 
	| key COLON NEWLINE INDENT
		 listEntry+
	  DEDENT
	;

listEntry:
	  LISTINDENT expr NEWLINE DEDENT
	| LISTINDENT
		mapEntry+ 
	  DEDENT 
	| LISTINDENT NEWLINE
		INDENT 
	    listEntry+
	  	DEDENT
	  DEDENT
	;

key:
  STR_PLAIN
  | STR_REGULAR
  ;

expr: 
	O_PAR expr C_PAR #parenthesis
	| expr DOT key #mapAccess
	| fkt=expr O_PAR (args+=expr) (LIST_SEP args+=expr)* LIST_SEP? C_PAR #application
    | args+=expr op=(MOD | DIV | POW | MULT | FLOOR_DIV | CEIL_DIV ) args+=expr #inlineBinary
	| args+=expr op=(ADD | SUB) args+=expr #inlineBinary
	| args+=expr op=(LESS | LESS_EQ | GREATER | GREATER_EQ) args+=expr #inlineBinary
	| args+=expr op=(EQ| NEQ) args+=expr #inlineBinary
	| args+=expr op=AND args+=expr #inlineBinary
	| args+=expr op=OR args+=expr #inlineBinary
	| BOOL #bool
	| SELF #self 
	| SELECTOR key #ref
	| sign=(SUB|ADD)? NUMBER #number
	| V_LINE (params+= STR_PLAIN) (LIST_SEP params+=STR_PLAIN)* LIST_SEP? V_LINE expr #lambda
	| STR_PLAIN #variable
	| STR_REGULAR #string
	| O_BRA (items+=expr (LIST_SEP items+=expr)* LIST_SEP?)? C_BRA #list
	;

