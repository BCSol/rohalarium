package bschuess.dev.rohalarium.h3xcore.parser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;

import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.BoolContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.DocumentContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.InlineBinaryContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.LambdaContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.ListEntryContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.MapAccessContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.MapEntryContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.NumberContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.RefContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.SelfContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.StringContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.VariableContext;

public class SemanticTokenVisitor extends H3xcoreParserBaseVisitor<Void> {

  public record SemanticToken(SemanticTokens annotation, int line, int inLineIndex, int lenght) {}

  private final List<SemanticToken> data = new ArrayList<>();

  public List<Integer> getTokens() {
    var result = new ArrayList<Integer>();
    int lastLine = 0;
    int lastInLineIndex = 0;

    Comparator<SemanticToken> sortByOccurence =
        Comparator.comparingInt(SemanticToken::line).thenComparingInt(SemanticToken::inLineIndex);
    Collections.sort(data, sortByOccurence);

    for (var token : data) {
      if (token.line != lastLine) {
        lastInLineIndex = 0; // reset char line index on newline
      }
      result.add(token.line - lastLine);
      lastLine = token.line;

      result.add(token.inLineIndex - lastInLineIndex);
      lastInLineIndex = token.inLineIndex;

      result.add(token.lenght);
      result.add(token.annotation.ordinal());
      result.add(0); // no modifiers
    }
    return result;
  }

  private void safe(SemanticTokens annotation, Token token) {
    data.add(
        new SemanticToken(
            annotation,
            token.getLine() - 1,
            token.getCharPositionInLine(),
            token.getStopIndex() + 1 - token.getStartIndex()));
  }

  private void safe(SemanticTokens annotation, ParserRuleContext token) {
    data.add(
        new SemanticToken(
            annotation,
            token.start.getLine() - 1,
            token.start.getCharPositionInLine(),
            token.start.getStopIndex() + 1 - token.stop.getStartIndex()));
  }

  @Override
  public Void visitDocument(DocumentContext ctx) {
    safe(SemanticTokens.CLASS, ctx.key());
    return super.visitDocument(ctx);
  }

  @Override
  public Void visitMapEntry(MapEntryContext ctx) {
    safe(SemanticTokens.KEYWORD, ctx.key());
    return super.visitMapEntry(ctx);
  }

  @Override
  public Void visitListEntry(ListEntryContext ctx) {
    safe(SemanticTokens.KEYWORD, ctx.LISTINDENT().getSymbol());
    return super.visitListEntry(ctx);
  }

  @Override
  public Void visitVariable(VariableContext ctx) {
    safe(SemanticTokens.VARIABLE, ctx);
    return super.visitVariable(ctx);
  }

  @Override
  public Void visitLambda(LambdaContext ctx) {
    for (var v : ctx.params) {
      safe(SemanticTokens.PARAMETER, v);
    }
    return visit(ctx.expr());
  }

  @Override
  public Void visitInlineBinary(InlineBinaryContext ctx) {
    safe(SemanticTokens.OPERATOR, ctx.op);
    return super.visitInlineBinary(ctx);
  }

  @Override
  public Void visitSelf(SelfContext ctx) {
    safe(SemanticTokens.VARIABLE, ctx);
    return super.visitSelf(ctx);
  }

  @Override
  public Void visitRef(RefContext ctx) {
    safe(SemanticTokens.CLASS, ctx);
    return null;
  }

  @Override
  public Void visitMapAccess(MapAccessContext ctx) {
    var res = visit(ctx.expr());
    safe(SemanticTokens.KEYWORD, ctx.key());
    return res;
  }

  @Override
  public Void visitBool(BoolContext ctx) {
    safe(SemanticTokens.ENUM, ctx);
    return super.visitBool(ctx);
  }

  @Override
  public Void visitString(StringContext ctx) {
    safe(SemanticTokens.STRING, ctx);
    return super.visitString(ctx);
  }

  @Override
  public Void visitNumber(NumberContext ctx) {
    safe(SemanticTokens.NUMBER, ctx);
    return super.visitNumber(ctx);
  }
}
