package bschuess.dev.rohalarium.h3xcore.compiler;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

import org.antlr.v4.runtime.ANTLRErrorListener;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.atn.ATNConfigSet;
import org.antlr.v4.runtime.dfa.DFA;
import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.DiagnosticSeverity;
import org.eclipse.lsp4j.Position;
import org.eclipse.lsp4j.Range;

public class H3xDiagnosticErrorListener implements ANTLRErrorListener {

  private boolean syntaxError = false;

  public boolean isSyntaxError() {
    return syntaxError;
  }

  public List<Diagnostic> getErrors() {
    return errors;
  }

  private final List<Diagnostic> errors = new ArrayList<>();

  @Override
  public void syntaxError(
      Recognizer<?, ?> recognizer,
      Object offendingSymbol,
      int line,
      int charPositionInLine,
      String msg,
      RecognitionException e) {
    syntaxError = true;

    var error = new Diagnostic();
    var position = new Position(line - 1, charPositionInLine);
    error.setRange(new Range(position, position));
    error.setSeverity(DiagnosticSeverity.Error);
    error.setMessage(msg);
    error.setSource("Antlr4-SyntaxError");
    errors.add(error);
  }

  @Override
  public void reportAmbiguity(
      Parser recognizer,
      DFA dfa,
      int startIndex,
      int stopIndex,
      boolean exact,
      BitSet ambigAlts,
      ATNConfigSet configs) {}

  @Override
  public void reportAttemptingFullContext(
      Parser recognizer,
      DFA dfa,
      int startIndex,
      int stopIndex,
      BitSet conflictingAlts,
      ATNConfigSet configs) {}

  @Override
  public void reportContextSensitivity(
      Parser recognizer,
      DFA dfa,
      int startIndex,
      int stopIndex,
      int prediction,
      ATNConfigSet configs) {}
}
