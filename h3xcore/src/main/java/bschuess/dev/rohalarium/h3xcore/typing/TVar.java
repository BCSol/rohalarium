package bschuess.dev.rohalarium.h3xcore.typing;

import static com.google.common.base.Preconditions.checkState;

import java.util.concurrent.atomic.AtomicLong;

import com.google.errorprone.annotations.FormatMethod;
import com.google.errorprone.annotations.FormatString;

public record TVar(Object id, Locator at, @FormatString String format, Object... args) {
  private static final AtomicLong typeVarCounter = new AtomicLong(0);

  private static Object getVar() {
    long v = typeVarCounter.incrementAndGet();
    checkState(v != 0, "Exhausted type var space.");
    return "T" + Long.toUnsignedString(v);
  }

  @FormatMethod
  public static TVar create(Locator at, @FormatString String format, Object... args) {
    return new TVar(getVar(), at, format, args);
  }

  public String toString() {
    return id.toString();
  }
}
