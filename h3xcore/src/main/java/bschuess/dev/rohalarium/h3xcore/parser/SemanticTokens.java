package bschuess.dev.rohalarium.h3xcore.parser;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.lsp4j.SemanticTokensLegend;

import com.google.common.collect.Lists;

public enum SemanticTokens {
  NUMBER("number"),
  STRING("string"),
  ENUM("enum"),

  OPERATOR("operator"),
  FUNCTION("function"),

  CLASS("class"),
  VARIABLE("variable"),
  PARAMETER("parameter"),
  KEYWORD("keyword");

  private final String token;

  private SemanticTokens(String token) {
    this.token = checkNotNull(token);
  }

  private static final List<String> legend =
      Lists.newArrayList(values()).stream()
          .map(e -> e.token)
          .collect(Collectors.toCollection(ArrayList::new));

  public static SemanticTokensLegend legend() {
    return new SemanticTokensLegend(legend, new ArrayList<>());
  }
}
