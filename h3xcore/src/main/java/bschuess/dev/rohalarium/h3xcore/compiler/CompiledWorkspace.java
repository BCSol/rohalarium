package bschuess.dev.rohalarium.h3xcore.compiler;

import java.net.URI;

import org.eclipse.lsp4j.Diagnostic;

import com.google.common.collect.Multimap;

import bschuess.dev.rohalarium.h3xcore.typing.TypeContext;

public record CompiledWorkspace(TypeContext ctx, Multimap<URI, Diagnostic> errors) {}
