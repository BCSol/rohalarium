package bschuess.dev.rohalarium.h3xcore.typing;

import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

import bschuess.dev.rohalarium.h3xcore.typing.types.H3xType;

public record TypeConstraint(TVar typeV, H3xType type) {

  public Optional<Supplier<TypeConstraint>> substitute(Map<TVar, TVar> substitution) {
    return type.substitute(substitution)
        .map(newTypeSupplier -> () -> new TypeConstraint(typeV, newTypeSupplier.get()));
  }

  public String toString() {
    return String.format("%s extends %s", typeV, type);
  }
}
