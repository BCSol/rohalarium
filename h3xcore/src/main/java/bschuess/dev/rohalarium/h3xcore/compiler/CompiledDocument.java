package bschuess.dev.rohalarium.h3xcore.compiler;

import java.net.URI;
import java.util.Optional;

import org.eclipse.lsp4j.Diagnostic;

import com.google.common.collect.Multimap;

import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.FileInputContext;
import bschuess.dev.rohalarium.h3xcore.typing.TypeContext;

public record CompiledDocument(
    URI uri,
    Optional<FileInputContext> fileInput,
    TypeContext tctx,
    Multimap<URI, Diagnostic> diagnostics,
    boolean hasSyntaxErrors,
    boolean hasTypeErrors) {}
