package bschuess.dev.rohalarium.h3xcore.parser;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.PrintStream;
import java.util.Iterator;
import java.util.stream.Collectors;

import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.TerminalNode;

import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.ApplicationContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.DocumentContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.ExprContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.FileInputContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.InlineBinaryContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.KeyContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.LambdaContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.ListContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.ListEntryContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.MapEntryContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.NumberContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.VariableContext;

public class FormattingASTVisitor extends H3xcoreParserBaseVisitor<Void> {

  private final PrintStream out;
  private int indent = 0;

  public FormattingASTVisitor(PrintStream out) {
    this.out = checkNotNull(out);
  }

  private void indents() {
    out.print(" ".repeat(indent * 2));
  }

  private void newline() {
    out.print("\n");
  }

  @Override
  public Void visitFileInput(FileInputContext ctx) {
    for (var i = 0; i < ctx.documents.size(); i++) {
      visitDocument(ctx.documents.get(i));
      if (i + 1 != ctx.documents.size()) {
        out.print("\n\n");
      }
    }
    return null;
  }

  @Override
  public Void visitDocument(DocumentContext ctx) {
    visitKey(ctx.key());
    visitTerminal(ctx.COLON());
    newline();
    indent++;
    ctx.mapEntry().forEach(this::visitMapEntry);
    indent--;
    return null;
  }

  @Override
  public Void visitMapEntry(MapEntryContext ctx) {
    visitMapEntry(ctx, true);
    return null;
  }

  private void visitMapEntry(MapEntryContext ctx, boolean writeIndent) {
    if (writeIndent) {
      indents();
    }
    visitKey(ctx.key());
    visitTerminal(ctx.COLON());
    if (ctx.mapEntry().size() > 0) {
      newline();
      indent++;
      ctx.mapEntry().forEach(this::visitMapEntry);
      indent--;
    } else if (ctx.listEntry().size() > 0) {
      newline();
      indent++;
      ctx.listEntry().forEach(this::visitListEntry);
      indent--;
    } else {
      out.print(" ");
      visit(ctx.expr());
      newline();
    }
  }

  @Override
  public Void visitListEntry(ListEntryContext ctx) {
    indents();
    out.print("-");
    if (ctx.mapEntry().size() > 0) {
      out.print(" ");
      indent++;
      visitMapEntry(ctx.mapEntry(0), false); //
      if (ctx.mapEntry().size() > 1) {
        ctx.mapEntry().subList(1, ctx.mapEntry().size()).forEach(this::visitMapEntry);
      }
      indent--;
    } else if (ctx.listEntry().size() > 0) {
      newline();
      indent++;
      ctx.listEntry().forEach(this::visitListEntry);
      indent--;
    } else {
      out.print(" ");
      visit(ctx.expr());
      newline();
    }
    return null;
  }

  @Override
  public Void visitInlineBinary(InlineBinaryContext ctx) {
    visit(ctx.args.get(0));
    out.print(" ");
    out.print(ctx.op.getText());
    out.print(" ");
    visit(ctx.args.get(1));
    return null;
  }

  @Override
  public Void visitList(ListContext ctx) {
    visitTerminal(ctx.O_BRA());
    Iterator<ExprContext> items = ctx.items.iterator();
    ExprContext item = null;
    while (items.hasNext()) {
      item = items.next();
      visit(item);
      out.print(",");
      if (items.hasNext()) {
        out.print(" ");
      }
    }
    visitTerminal(ctx.C_BRA());
    return null;
  }

  @Override
  public Void visitKey(KeyContext ctx) {
    if (ctx.STR_PLAIN() != null) {
      visitTerminal(ctx.STR_PLAIN());
    } else if (ctx.STR_REGULAR() != null) {
      visitTerminal(ctx.STR_REGULAR());
    }
    return null;
  }

  @Override
  public Void visitVariable(VariableContext ctx) {
    return visitTerminal(ctx.STR_PLAIN());
  }

  @Override
  public Void visitLambda(LambdaContext ctx) {
    visitTerminal(ctx.V_LINE(0));
    var params = ctx.params.stream().map(Token::getText).collect(Collectors.joining(", "));
    out.print(params);
    visitTerminal(ctx.V_LINE(1));
    out.print(" ");
    return visit(ctx.expr());
  }

  @Override
  public Void visitApplication(ApplicationContext ctx) {
    visit(ctx.fkt);
    visitTerminal(ctx.O_PAR());
    for (var i = 0; i < ctx.args.size(); i++) {
      visit(ctx.args.get(i));
      // omit separator on last entry
      if (i + 1 != ctx.args.size()) {
        out.print(", ");
      }
    }

    visitTerminal(ctx.C_PAR());
    return null;
  }

  @Override
  public Void visitNumber(NumberContext ctx) {
    if (ctx.sign != null) {
      out.print(ctx.sign.getText());
    }
    visitTerminal(ctx.NUMBER());
    return null;
  }

  @Override
  public Void visitTerminal(TerminalNode node) {
    out.print(node.getSymbol().getText());
    return null;
  }
}
