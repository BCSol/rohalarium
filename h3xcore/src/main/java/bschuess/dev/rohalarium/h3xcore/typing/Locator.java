package bschuess.dev.rohalarium.h3xcore.typing;

import static com.google.common.base.Preconditions.checkNotNull;

import java.net.URI;
import java.util.function.Supplier;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.eclipse.lsp4j.Location;
import org.eclipse.lsp4j.Position;
import org.eclipse.lsp4j.Range;

import bschuess.dev.rohalarium.h3xcore.utils.Ranges;

public record Locator(URI document, Range range, Supplier<String> text) {

  public Locator {
    checkNotNull(document);
    checkNotNull(range);
    checkNotNull(text);
  }

  public String toString() {
    return String.format(
        "%s:%s-%s:%s@'%s'",
        range.getStart().getLine(),
        range.getStart().getCharacter(),
        range.getEnd().getLine(),
        range.getEnd().getCharacter(),
        document);
  }

  public Location toLocation() {
    return new Location(document.toString(), range);
  }

  public static Locator from(URI document, ParserRuleContext ctx) {
    return new Locator(document, Ranges.from(ctx), ctx::getText);
  }

  public static Locator from(URI document, Token token) {
    return new Locator(document, Ranges.from(token), token::getText);
  }

  public static Locator stdlib(URI doc, String signature) {
    return new Locator(doc, new Range(new Position(0, 0), new Position(0, 0)), signature::toString);
  }
}
