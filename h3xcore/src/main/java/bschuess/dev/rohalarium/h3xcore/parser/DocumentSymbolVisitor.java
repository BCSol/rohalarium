package bschuess.dev.rohalarium.h3xcore.parser;

import java.util.ArrayList;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.antlr.v4.runtime.tree.ParseTree;
import org.eclipse.lsp4j.DocumentSymbol;
import org.eclipse.lsp4j.SymbolKind;

import com.google.common.collect.Streams;

import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.DocumentContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.LambdaContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.ListEntryContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.MapEntryContext;
import bschuess.dev.rohalarium.h3xcore.utils.Parser;
import bschuess.dev.rohalarium.h3xcore.utils.Ranges;

public class DocumentSymbolVisitor extends H3xcoreParserBaseVisitor<DocumentSymbol> {

  private void setChildren(DocumentSymbol symbol, Stream<? extends ParseTree> stream) {
    var children =
        stream
            .filter(Objects::nonNull)
            .map(this::visit)
            .filter(Objects::nonNull)
            .collect(Collectors.toCollection(ArrayList::new));
    symbol.setChildren(children);
  }

  @Override
  public DocumentSymbol visitDocument(DocumentContext ctx) {
    var symbol =
        new DocumentSymbol(
            Parser.parseKey(ctx.key()), SymbolKind.Class, Ranges.from(ctx), Ranges.from(ctx.key()));
    setChildren(symbol, ctx.mapEntry().stream());
    return symbol;
  }

  @Override
  public DocumentSymbol visitMapEntry(MapEntryContext ctx) {
    var symbol =
        new DocumentSymbol(
            Parser.parseKey(ctx.key()),
            ctx.expr() instanceof LambdaContext ? SymbolKind.Method : SymbolKind.Field,
            Ranges.from(ctx),
            Ranges.from(ctx.key()));

    if (!ctx.listEntry().isEmpty()) {
      var children =
          IntStream.range(0, ctx.listEntry().size())
              .mapToObj(i -> visitListEntry(i, ctx.listEntry(i)))
              .collect(Collectors.toCollection(ArrayList::new));
      symbol.setChildren(children);
    } else {
      setChildren(
          symbol, Streams.<ParseTree>concat(Stream.of(ctx.expr()), ctx.mapEntry().stream()));
    }

    return symbol;
  }

  public DocumentSymbol visitListEntry(int index, ListEntryContext ctx) {
    var symbol =
        new DocumentSymbol(
            String.format("[%s]", index),
            SymbolKind.Array,
            Ranges.from(ctx),
            Ranges.from(ctx.LISTINDENT().getSymbol()));

    if (!ctx.listEntry().isEmpty()) {
      var children =
          IntStream.range(0, ctx.listEntry().size())
              .mapToObj(i -> visitListEntry(i, ctx.listEntry(i)))
              .collect(Collectors.toCollection(ArrayList::new));
      symbol.setChildren(children);
    } else {
      setChildren(
          symbol, Streams.<ParseTree>concat(Stream.of(ctx.expr()), ctx.mapEntry().stream()));
    }

    setChildren(symbol, Stream.of(ctx.expr()));
    return symbol;
  }
}
