package bschuess.dev.rohalarium.h3xcore.typing.types;

import com.google.common.collect.ImmutableSet;

public class H3xString implements H3xPrimitive {

  public static final H3xString STRING = new H3xString();

  private static final ImmutableSet<H3xOverloadable> supported =
      ImmutableSet.of(H3xOverloadable.COMP);

  private H3xString() {}

  @Override
  public boolean supports(H3xOverloadable overloadable) {
    return supported.contains(overloadable);
  }

  @Override
  public java.lang.String toString() {
    return getClass().getSimpleName();
  }
}
