package bschuess.dev.rohalarium.h3xcore.typing;

import java.net.URI;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import bschuess.dev.rohalarium.h3xcore.parser.TypeCollector;
import bschuess.dev.rohalarium.h3xcore.typing.types.H3xBool;
import bschuess.dev.rohalarium.h3xcore.typing.types.H3xFunction;
import bschuess.dev.rohalarium.h3xcore.typing.types.H3xNumber;
import bschuess.dev.rohalarium.h3xcore.typing.types.H3xOverloadable;
import bschuess.dev.rohalarium.h3xcore.typing.types.H3xPrimitive;
import bschuess.dev.rohalarium.h3xcore.typing.types.H3xType;

public class Stdlib {

  public static final URI URI = java.net.URI.create("Stdlib:stdlib");
  public static final Stdlib STD_LIB = new Stdlib();

  public final TypeContext TYPE_CONTEXT;
  public final ImmutableMap<String, TVar> STD_OPS;

  private Stdlib() {
    var tctx = new TypeCollector();

    var builder = ImmutableMap.<String, TVar>builder();

    var add = "+";
    builder.put(add, stbopwc(tctx, add, H3xOverloadable.ADD));

    var greater = ">";
    builder.put(greater, bopcomp(tctx, greater));
    var greaterEq = ">=";
    builder.put(greaterEq, bopcomp(tctx, greaterEq));
    var lessEq = "<=";
    builder.put(lessEq, bopcomp(tctx, lessEq));
    var less = "<";
    builder.put(less, bopcomp(tctx, less));

    var mod = "%";
    builder.put(mod, bopwmt(tctx, mod, H3xNumber.NUMBER));
    var div = "/";
    builder.put(div, bopwmt(tctx, div, H3xNumber.NUMBER));
    var pow = "**";
    builder.put(pow, bopwmt(tctx, pow, H3xNumber.NUMBER));
    var mult = "*";
    builder.put(mult, bopwmt(tctx, mult, H3xNumber.NUMBER));
    var floorDiv = "//";
    builder.put(floorDiv, bopwmt(tctx, floorDiv, H3xNumber.NUMBER));
    var ceilDiv = "///";
    builder.put(ceilDiv, bopwmt(tctx, ceilDiv, H3xNumber.NUMBER));
    var sub = "-";
    builder.put(sub, bopwmt(tctx, sub, H3xNumber.NUMBER));

    var and = "and";
    builder.put(and, bopwmt(tctx, and, H3xBool.BOOL));
    var or = "or";
    builder.put(or, bopwmt(tctx, or, H3xBool.BOOL));

    var eq = "==";
    builder.put(eq, eqops(tctx, eq));
    var neq = "!=";
    builder.put(neq, eqops(tctx, neq));

    STD_OPS = builder.build();
    TYPE_CONTEXT = tctx.result();
  }

  private static TVar eqops(TypeCollector tctx, String op) {
    var signature = String.format("%s :: FKT<?,?,BOOL>", op);
    var opL = Locator.stdlib(URI, signature);
    var opV = TVar.create(opL, signature);

    TVar leftV = TVar.create(opL, "%s param left", op);
    TVar rightV = TVar.create(opL, "%s param right", op);
    TVar resultV = TVar.create(opL, "%s result", op);
    tctx.registerStdLibType(resultV, H3xBool.BOOL, opL);
    var opT = new H3xFunction(resultV, ImmutableList.of(leftV, rightV));
    tctx.registerStdLibType(opV, opT, opL);
    return opV;
  }

  /** Return binary operation with constrained monotype. */
  private static TVar stbopwc(TypeCollector tctx, String op, H3xType constraint) {
    var signature = String.format("%s :: FKT<T,T,T> where T extends %s", op, constraint);

    var opL = Locator.stdlib(URI, signature);
    var opV = TVar.create(opL, signature);

    TVar monoType = TVar.create(opL, signature);
    tctx.registerTypeConstraint(monoType, constraint);

    var opT = new H3xFunction(monoType, ImmutableList.of(monoType, monoType));
    tctx.registerStdLibType(opV, opT, opL);
    return opV;
  }

  /** Return binary operation with actual monotype */
  private static TVar bopwmt(TypeCollector tctx, String op, H3xPrimitive type) {
    return bopwt(tctx, op, type, type, type);
  }

  /** Return binary operation with actual types */
  private static TVar bopwt(
      TypeCollector tctx, String op, H3xType left, H3xType right, H3xPrimitive result) {
    var signature = String.format("%s :: FKT<%s,%s,%s>", op, left, right, result);
    var opL = Locator.stdlib(URI, signature);
    var opV = TVar.create(opL, signature);

    TVar leftV = TVar.create(opL, "%s param left", op);
    tctx.registerStdLibType(leftV, left, opL);
    TVar rightV = TVar.create(opL, "%s param right", op);
    tctx.registerStdLibType(rightV, right, opL);
    TVar resultV = TVar.create(opL, "%s result", op);
    tctx.registerStdLibType(resultV, result, opL);

    var opT = new H3xFunction(resultV, ImmutableList.of(leftV, rightV));
    tctx.registerStdLibType(opV, opT, opL);
    return opV;
  }

  private static TVar bopcomp(TypeCollector tctx, String op) {
    var signature = String.format("%s :: FKT<T,T,BOOL> where T extends COMP", op);
    var opL = Locator.stdlib(URI, signature);
    var opV = TVar.create(opL, signature);

    TVar paramType = TVar.create(opL, signature);
    tctx.registerTypeConstraint(paramType, H3xOverloadable.COMP);
    TVar resultType = TVar.create(opL, signature);
    tctx.registerStdLibType(resultType, H3xBool.BOOL, opL);

    var opT = new H3xFunction(resultType, ImmutableList.of(paramType, paramType));
    tctx.registerStdLibType(opV, opT, opL);
    return opV;
  }
}
