package bschuess.dev.rohalarium.h3xcore.typing;

public record TypeVarInheritance(TVar subTypeV, TVar superTypeV) {

  public String toString() {
    return String.format("%s extends %s", subTypeV, superTypeV);
  }
}
