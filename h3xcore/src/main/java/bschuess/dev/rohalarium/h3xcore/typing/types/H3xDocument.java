package bschuess.dev.rohalarium.h3xcore.typing.types;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.base.Joiner;
import com.google.common.base.Joiner.MapJoiner;

public class H3xDocument extends H3xMap {

  private final String name;

  public H3xDocument(String name) {
    this.name = checkNotNull(name);
  }

  public String name() {
    return name;
  }

  @Override
  protected H3xMap newInstance() {
    return new H3xDocument(name);
  }

  @Override
  public boolean equalsType(H3xType type) {
    if (type instanceof H3xDocument other) {
      return this.name.equals(other.name);
    }
    return false;
  }

  @Override
  public String toString() {
    MapJoiner joiner = Joiner.on(',').withKeyValueSeparator(':');
    return String.format("'%s'<%s>", name, joiner.join(entryTypes));
  }
}
