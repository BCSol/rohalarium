package bschuess.dev.rohalarium.h3xcore.typing.types;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Stream;

import com.google.common.base.Joiner;
import com.google.common.base.Joiner.MapJoiner;

import bschuess.dev.rohalarium.h3xcore.typing.TVar;

public class H3xMap implements H3xType {
  protected final Map<String, TVar> entryTypes = new HashMap<>();

  // may only used during actual type generation
  public void put(String key, TVar type) {
    entryTypes.put(key, type);
  }

  public Map<String, TVar> entries() {
    return Collections.unmodifiableMap(entryTypes);
  }

  @Override
  public boolean equalsType(H3xType type) {
    if (type instanceof H3xMap other) {
      return Stream.concat(this.entryTypes.keySet().stream(), other.entryTypes.keySet().stream())
          .distinct()
          .allMatch(key -> Objects.equals(this.entryTypes.get(key), other.entryTypes.get(key)));
    }
    return false;
  }

  @Override
  public Optional<Supplier<H3xType>> substitute(Map<TVar, TVar> substitution) {
    var doSubstitution = entryTypes.values().stream().anyMatch(substitution::containsKey);
    if (doSubstitution) {
      return Optional.of(
          () -> {
            var newType = newInstance();
            entryTypes.forEach(
                (k, v) -> {
                  newType.put(k, substitution.getOrDefault(v, v));
                });
            return newType;
          });
    }
    return Optional.empty();
  }

  protected H3xMap newInstance() {
    return new H3xMap();
  }

  @Override
  public String toString() {
    MapJoiner joiner = Joiner.on(", ").withKeyValueSeparator(" :: ");
    return String.format("Map{%s}", joiner.join(entryTypes));
  }
}
