package bschuess.dev.rohalarium.h3xcore.typing.types;

import static com.google.common.base.Preconditions.checkNotNull;

public enum H3xOverloadable implements H3xType {
  ADD("ADD"),
  COMP("COMP");
  private final java.lang.String operation;

  private H3xOverloadable(java.lang.String string) {
    this.operation = checkNotNull(string);
  }

  @Override
  public boolean supports(H3xOverloadable overloadable) {
    return false; // must never become actual type :)
  }

  @Override
  public boolean equalsType(H3xType type) {
    if (type instanceof H3xOverloadable other) {
      return this.ordinal() == other.ordinal();
    }
    return false;
  }

  @Override
  public java.lang.String toString() {
    return operation;
  }
}
