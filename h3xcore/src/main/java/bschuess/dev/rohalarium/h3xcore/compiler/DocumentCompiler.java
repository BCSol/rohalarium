package bschuess.dev.rohalarium.h3xcore.compiler;

import java.net.URI;
import java.util.Optional;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.eclipse.lsp4j.Diagnostic;

import com.google.common.collect.MultimapBuilder;

import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreLexer;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser;
import bschuess.dev.rohalarium.h3xcore.parser.SyntaxErrorCheckerVisitor;
import bschuess.dev.rohalarium.h3xcore.parser.TypeCollectorVisitor;
import bschuess.dev.rohalarium.h3xcore.typing.Stdlib;
import bschuess.dev.rohalarium.h3xcore.typing.TypeContext;

public class DocumentCompiler {

  public static CompiledDocument compile(URI uri, String source) {
    var input = CharStreams.fromString(source);
    return compile(uri, input);
  }

  public static CompiledDocument compile(URI uri, CharStream input) {
    if (input.size() == 0) {
      return new CompiledDocument(
          uri,
          Optional.empty(),
          TypeContext.EMPTY,
          MultimapBuilder.hashKeys().arrayListValues().build(),
          false,
          false);
    }
    var lexer = new H3xcoreLexer(input);
    var tokens = new CommonTokenStream(lexer);
    var parser = new H3xcoreParser(tokens);
    var errors = MultimapBuilder.hashKeys().arrayListValues().<URI, Diagnostic>build();

    // TODO parser error handler to recover from syntax errors
    var errorListener = new H3xDiagnosticErrorListener();
    parser.addErrorListener(errorListener);

    var fileInput = parser.fileInput();
    var syntaxError = errorListener.isSyntaxError();
    errors.putAll(uri, errorListener.getErrors());

    if (syntaxError) {
      return new CompiledDocument(uri, Optional.empty(), TypeContext.EMPTY, errors, true, false);
    }

    // some static analysis
    var syntaxChecker = new SyntaxErrorCheckerVisitor(uri, errors);
    syntaxError |= syntaxChecker.visit(fileInput);
    if (syntaxError) {
      return new CompiledDocument(
          uri, Optional.of(fileInput), TypeContext.EMPTY, errors, true, false);
    }

    boolean typeErrors = false;
    var tctx = TypeContext.EMPTY;
    var typeCollector = new TypeCollectorVisitor(uri);
    typeCollector.visit(fileInput);
    tctx = typeCollector.result();
    tctx.inject(Stdlib.STD_LIB);
    typeErrors |= tctx.compile(errors);

    var result =
        new CompiledDocument(uri, Optional.of(fileInput), tctx, errors, syntaxError, typeErrors);
    return result;
  }
}
