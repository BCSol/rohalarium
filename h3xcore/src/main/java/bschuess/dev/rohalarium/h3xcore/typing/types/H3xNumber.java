package bschuess.dev.rohalarium.h3xcore.typing.types;

import com.google.common.collect.ImmutableSet;

public class H3xNumber implements H3xPrimitive {

  public static final H3xNumber NUMBER = new H3xNumber();

  private static final ImmutableSet<H3xOverloadable> supported =
      ImmutableSet.of(H3xOverloadable.ADD, H3xOverloadable.COMP);

  private H3xNumber() {}

  @Override
  public boolean supports(H3xOverloadable overloadable) {
    return supported.contains(overloadable);
  }

  @Override
  public java.lang.String toString() {
    return getClass().getSimpleName();
  }
}
