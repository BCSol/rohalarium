package bschuess.dev.rohalarium.h3xcore.parser;

import java.util.LinkedList;
import java.util.Stack;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonToken;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.Token;

abstract class IndentDedentLexerBase extends Lexer {

  protected IndentDedentLexerBase(CharStream input) {
    super(input);
  }

  private LinkedList<Token> pendingTokens = new LinkedList<>();
  private Stack<Integer> indentCounts = new Stack<>();

  // last token on default channel
  private Token lastToken = null;

  @Override
  public Token nextToken() {
    Token next;
    if (pendingTokens.isEmpty()) {
      next = super.nextToken();
      if (next.getType() == H3xcoreLexer.EOF) {
        // insert newline for statement completion
        pendingTokens.offer(createClosingToken(H3xcoreLexer.NEWLINE));
        // emit needed DEDENT tokens
        while (!indentCounts.isEmpty()) {
          pendingTokens.offer(createClosingToken(H3xcoreLexer.DEDENT));
          indentCounts.pop();
        }
        pendingTokens.offer(next); // offer EOF
        next = pendingTokens.poll();
      }
    } else {
      next = pendingTokens.poll();
    }

    if (next != null && next.getChannel() == Token.DEFAULT_CHANNEL) {
      // keep track of last token on the default channel for DEDENT creation.
      this.lastToken = next;
    }
    return next;
  }

  boolean atStartOfInput() {
    return super.getCharPositionInLine() == 0 && super.getLine() == 1;
  }

  void onNewLine() {
    String text = getText();
    String newline = text.replaceAll("[^\t\n\f]", "");

    int bullet = text.indexOf('-');
    String spaces = (bullet != -1 ? text.substring(0, bullet) : text).replaceAll("[\t\n\f]", "");
    String listSpaces = bullet != -1 ? text.substring(bullet) : "";

    int next = _input.LA(1);
    int nextnext = _input.LA(2);

    if (listSpaces.isEmpty() // '-' counts not as empty line
        && nextnext != -1 // EOF
        && (next == '\r' || next == '\n' || next == '\f')) {
      // skip indents/dedents/newlines in blank line
      skip();
    } else {
      // we emit this instead of offer it, to make super.nextToken return this
      // to replace the default newline to not receive duplicates
      emit(commonToken(H3xcoreLexer.NEWLINE, 0, newline.length()));

      int previous = indentCounts.isEmpty() ? 0 : indentCounts.peek();
      int indent = getIndentationCount(spaces);
      if (indent == previous) {
        if (!listSpaces.isEmpty()) {
          // indent if list indent is found
          int listIndent = getIndentationCount(spaces + listSpaces);
          pendingTokens.offer(
              commonToken(
                  H3xcoreLexer.LISTINDENT,
                  newline.length() + spaces.length(),
                  listSpaces.length()));
          indentCounts.push(listIndent);
        }
      } else if (indent > previous) {
        indentCounts.push(indent);
        pendingTokens.offer(commonToken(H3xcoreLexer.INDENT, newline.length(), spaces.length()));
        // indent if list indent is found
        if (!listSpaces.isEmpty()) {
          int listIndent = getIndentationCount(spaces + listSpaces);
          pendingTokens.offer(
              commonToken(
                  H3xcoreLexer.LISTINDENT,
                  newline.length() + spaces.length(),
                  listSpaces.length()));
          indentCounts.push(listIndent);
        }
      } else {
        // potentially emit more than 1 DEDENT token
        while (!indentCounts.isEmpty() && indentCounts.peek() > indent) {
          pendingTokens.offer(createClosingToken(H3xcoreLexer.DEDENT));
          indentCounts.pop();
        }
        // since we ignore blank lines we will always end up at a matched indentation
        // level
        // if they don't match it indicates an indentation error.
        if (!listSpaces.isEmpty()) { // re-indent if necessary
          int listIndent = getIndentationCount(spaces + listSpaces);
          pendingTokens.offer(
              commonToken(
                  H3xcoreLexer.LISTINDENT,
                  newline.length() + spaces.length(),
                  listSpaces.length()));
          indentCounts.push(listIndent);
        }
      }
    }
  }

  // calculates indentation from str of tabs and spaces and '-'
  static int getIndentationCount(String spaces) {
    int count = 0;
    for (char ch : spaces.toCharArray()) {
      switch (ch) {
        case '\t':
          count += 2 - (count % 2);
          break;
        default:
          count += 1;
      }
    }
    return count;
  }

  private Token createClosingToken(int type) {
    String text = getText();
    CommonToken dedent = commonToken(type, text.length() - 1, 1);
    dedent.setLine(this.lastToken.getLine());
    return dedent;
  }

  // don't call with empty text
  private CommonToken commonToken(int type, int indexInGetText, int lenText) {
    int start = this.getCharIndex() - getText().length() + indexInGetText;
    int stop = start + lenText - 1; // inclusive stop
    var token =
        new CommonToken(this._tokenFactorySourcePair, type, DEFAULT_TOKEN_CHANNEL, start, stop);
    int start2 = this.getCharPositionInLine() - lenText;
    token.setCharPositionInLine(start2);
    return token;
  }
}
