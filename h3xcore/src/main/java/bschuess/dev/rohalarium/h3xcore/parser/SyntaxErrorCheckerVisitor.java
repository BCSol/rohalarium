package bschuess.dev.rohalarium.h3xcore.parser;

import static com.google.common.base.Preconditions.checkNotNull;

import java.net.URI;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.DiagnosticRelatedInformation;
import org.eclipse.lsp4j.DiagnosticSeverity;
import org.eclipse.lsp4j.Location;
import org.eclipse.lsp4j.Range;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;

import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.DocumentContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.KeyContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.LambdaContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.ListEntryContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.MapEntryContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.VariableContext;
import bschuess.dev.rohalarium.h3xcore.typing.Locator;
import bschuess.dev.rohalarium.h3xcore.utils.Parser;
import bschuess.dev.rohalarium.h3xcore.utils.Ranges;

public class SyntaxErrorCheckerVisitor extends H3xcoreParserBaseVisitor<Boolean> {

  private final URI document;
  private final Multimap<URI, Diagnostic> errorSink;
  private ImmutableMap<String, Range> varScope = ImmutableMap.of();

  public SyntaxErrorCheckerVisitor(URI document, Multimap<URI, Diagnostic> errorSink) {
    this.document = checkNotNull(document);
    this.errorSink = checkNotNull(errorSink);
  }

  @Override
  protected Boolean defaultResult() {
    return false;
  }

  @Override
  protected Boolean aggregateResult(Boolean aggregate, Boolean nextResult) {
    return aggregate || nextResult;
  }

  private Boolean checkMapEntries(List<MapEntryContext> entries) {
    var keys = new HashMap<String, KeyContext>();
    var triggeredError = new HashSet<KeyContext>();
    var error = false;
    for (var mapEntryCtx : entries) {
      var keyCtx = mapEntryCtx.key();
      var key = Parser.parseKey(keyCtx);
      var oldKeyCtx = keys.put(key, keyCtx);
      if (oldKeyCtx != null) {
        error |= true;
        if (triggeredError.add(keyCtx)) {
          createDuplicateKeyError(key, keyCtx);
        }
        if (triggeredError.add(oldKeyCtx)) {
          createDuplicateKeyError(key, oldKeyCtx);
        }
      }
    }
    return error;
  }

  private void createDuplicateKeyError(String key, KeyContext keyCtx) {
    var diagnostic =
        new Diagnostic(
            Locator.from(document, keyCtx).range(),
            String.format("duplicate key '%s'", key),
            DiagnosticSeverity.Error,
            "SyntaxError");
    errorSink.put(document, diagnostic);
  }

  @Override
  public Boolean visitDocument(DocumentContext ctx) {
    boolean error = checkMapEntries(ctx.mapEntry());
    error |= super.visitDocument(ctx);
    return error;
  }

  @Override
  public Boolean visitMapEntry(MapEntryContext ctx) {
    boolean error = checkMapEntries(ctx.mapEntry());
    error |= super.visitMapEntry(ctx);
    return error;
  }

  @Override
  public Boolean visitListEntry(ListEntryContext ctx) {
    boolean error = checkMapEntries(ctx.mapEntry());
    error |= super.visitListEntry(ctx);
    return error;
  }

  @Override
  public Boolean visitLambda(LambdaContext ctx) {
    var oldScope = this.varScope;
    var newScope = new HashMap<String, Range>();
    var definedParams = new HashMap<String, Range>();
    var error = false;
    newScope.putAll(oldScope);
    for (var param : ctx.params) {
      var name = param.getText();
      var range = Ranges.from(param);
      if (oldScope.containsKey(name)) {
        var diagnostic =
            new Diagnostic(
                range,
                "Shadowing variable from outer scope.",
                DiagnosticSeverity.Warning,
                "H3xAnalysis");
        diagnostic.setRelatedInformation(
            Lists.newArrayList(
                new DiagnosticRelatedInformation(
                    new Location(document.toString(), oldScope.get(name)),
                    String.format("'%s' is also defined here.", name))));
        errorSink.put(document, diagnostic);
      }
      if (definedParams.containsKey(name)) {
        error |= true;
        var diagnostic =
            new Diagnostic(
                range,
                String.format("Duplicate variable '%s'", name),
                DiagnosticSeverity.Error,
                "SyntaxError");
        diagnostic.setRelatedInformation(
            Lists.newArrayList(
                new DiagnosticRelatedInformation(
                    new Location(document.toString(), definedParams.get(name)),
                    String.format("'%s' is already defined here.", name))));
        errorSink.put(document, diagnostic);
      }
      definedParams.put(name, range);
      newScope.put(name, range);
    }
    this.varScope = ImmutableMap.copyOf(newScope);
    error |= visit(ctx.expr());
    this.varScope = oldScope;
    return error;
  }

  @Override
  public Boolean visitVariable(VariableContext ctx) {
    if (varScope.containsKey(ctx.getText())) {
      return false;
    }
    var diagnostic =
        new Diagnostic(
            Ranges.from(ctx),
            String.format("Variable '%s' is undefined", ctx.getText()),
            DiagnosticSeverity.Error,
            "SyntaxError");
    errorSink.put(document, diagnostic);

    return true;
  }
}
