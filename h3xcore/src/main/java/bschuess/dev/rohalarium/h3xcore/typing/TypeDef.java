package bschuess.dev.rohalarium.h3xcore.typing;

import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

import bschuess.dev.rohalarium.h3xcore.typing.types.H3xType;

public record TypeDef(TVar typeV, H3xType type, Locator location) implements ResolvedType {

  public Optional<Supplier<ResolvedType>> substitute(Map<TVar, TVar> substitution) {
    return type.substitute(substitution)
        .map(newTypeSupplier -> () -> new TypeDef(typeV, newTypeSupplier.get(), location));
  }

  public String toString() {
    return String.format("%s :: %s", typeV, type);
  }
}
