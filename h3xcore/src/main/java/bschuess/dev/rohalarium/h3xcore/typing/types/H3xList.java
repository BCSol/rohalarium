package bschuess.dev.rohalarium.h3xcore.typing.types;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

import bschuess.dev.rohalarium.h3xcore.typing.TVar;

public record H3xList(TVar itemTVar) implements H3xType {

  public H3xList {
    checkNotNull(itemTVar);
  }

  @Override
  public Optional<Supplier<H3xType>> substitute(Map<TVar, TVar> substitution) {
    if (substitution.containsKey(itemTVar)) {
      return Optional.of(
          () -> {
            return new H3xList(substitution.get(itemTVar));
          });
    }
    return Optional.empty();
  }

  @Override
  public boolean equalsType(H3xType type) {
    if (type instanceof H3xList other) {
      return this.itemTVar.equals(other.itemTVar);
    }
    return false;
  }

  @Override
  public String toString() {
    return String.format("List<%s>", itemTVar);
  }
}
