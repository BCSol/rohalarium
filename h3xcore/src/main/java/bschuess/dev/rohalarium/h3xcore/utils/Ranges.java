package bschuess.dev.rohalarium.h3xcore.utils;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.eclipse.lsp4j.Position;
import org.eclipse.lsp4j.Range;

public class Ranges {

  public static Range from(ParserRuleContext ctx) {
    return from(ctx.start, ctx.stop);
  }

  public static Range from(Token token) {
    return from(token, token);
  }

  public static Range from(Token startToken, Token endToken) {
    // FIXME adjust for newline == empty? and UTF-8/16 double chars.
    var start = new Position(startToken.getLine() - 1, startToken.getCharPositionInLine());
    var stop =
        new Position(
            endToken.getLine() - 1, endToken.getCharPositionInLine() + endToken.getText().length());
    return new Range(start, stop);
  }
}
