package bschuess.dev.rohalarium.h3xcore.parser;

import static com.google.common.base.Preconditions.checkState;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.ParserRuleContext;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import com.google.errorprone.annotations.FormatMethod;

import bschuess.dev.rohalarium.h3xcore.typing.Locator;
import bschuess.dev.rohalarium.h3xcore.typing.TVar;
import bschuess.dev.rohalarium.h3xcore.typing.TypeConstraint;
import bschuess.dev.rohalarium.h3xcore.typing.TypeContext;
import bschuess.dev.rohalarium.h3xcore.typing.TypeDef;
import bschuess.dev.rohalarium.h3xcore.typing.TypeVarInheritance;
import bschuess.dev.rohalarium.h3xcore.typing.types.H3xFunction;
import bschuess.dev.rohalarium.h3xcore.typing.types.H3xType;

public class TypeCollector {

  private final Map<ParserRuleContext, TVar> context = new HashMap<>();

  private final Multimap<String, TVar> namedTypeDefs =
      MultimapBuilder.hashKeys().arrayListValues().build();
  private final Multimap<String, TVar> namedTypeRefs =
      MultimapBuilder.hashKeys().arrayListValues().build();

  private final List<TypeDef> types = new ArrayList<>();
  private final List<TypeConstraint> constraints = new ArrayList<>();
  private final List<TypeVarInheritance> varInheritance = new ArrayList<>();

  @FormatMethod
  private TVar registerContext(Locator at, ParserRuleContext expr) {
    checkState(!context.containsKey(expr), "Cannot register context for %s twice.");
    var v = TVar.create(at, "TypeDefinition");
    context.put(expr, v);
    return v;
  }

  //  public TVar getTVar(ParserRuleContext expr) {
  //    var v = context.get(expr);
  //    checkState(v != null, "No type var registered for %s in type context.", expr);
  //    return v;
  //  }

  /** Only use for real types. */
  public TVar registerTypeDefinition(URI document, ParserRuleContext expr, H3xType type) {
    var at = Locator.from(document, expr);
    return registerTypeDefinition(at, expr, type);
  }

  public TVar registerTypeDefinition(Locator at, ParserRuleContext expr, H3xType type) {
    var v = registerContext(at, expr);
    types.add(new TypeDef(v, type, at));
    return v;
  }

  public void registerNamedTypeDef(String name, TVar var) {
    namedTypeDefs.put(name, var);
  }

  public void registerNamedTypeRef(String name, TVar var) {
    namedTypeRefs.put(name, var);
  }

  public void registerStdLibType(TVar libFkt, H3xType type, Locator location) {
    types.add(new TypeDef(libFkt, type, location));
  }

  @FormatMethod
  public void registerTypeVarInheritance(TVar superTypeVar, TVar subTypeVar) {
    var rel = new TypeVarInheritance(subTypeVar, superTypeVar);
    varInheritance.add(rel);
  }

  @FormatMethod
  public void registerTypeConstraint(TVar tv, H3xType bound) {
    constraints.add(new TypeConstraint(tv, bound));
  }

  @FormatMethod
  public void registerApplicationConstraint(
      TVar fktTVar, TVar resultTVar, ImmutableList<TVar> fktArgs) {
    // this just works like instantiation of the function
    registerTypeConstraint(fktTVar, new H3xFunction(resultTVar, fktArgs));
  }

  public TypeContext result() {
    return TypeContext.from(
        context, types, constraints, varInheritance, namedTypeDefs, namedTypeRefs);
  }
}
