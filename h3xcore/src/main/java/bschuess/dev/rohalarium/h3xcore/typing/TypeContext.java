package bschuess.dev.rohalarium.h3xcore.typing;

import static com.google.common.base.Preconditions.checkState;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.antlr.v4.runtime.ParserRuleContext;
import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.DiagnosticRelatedInformation;
import org.eclipse.lsp4j.DiagnosticSeverity;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;

import bschuess.dev.rohalarium.h3xcore.typing.types.H3xDocument;
import bschuess.dev.rohalarium.h3xcore.typing.types.H3xFunction;
import bschuess.dev.rohalarium.h3xcore.typing.types.H3xList;
import bschuess.dev.rohalarium.h3xcore.typing.types.H3xMap;
import bschuess.dev.rohalarium.h3xcore.typing.types.H3xOverloadable;
import bschuess.dev.rohalarium.h3xcore.typing.types.H3xType;

public class TypeContext {

  public static final String SOURCE = "TypeChecker";
  public static final TypeContext EMPTY = new TypeContext();

  private TypeContext() {}

  public static TypeContext from(
      Map<ParserRuleContext, TVar> context,
      List<TypeDef> types,
      List<TypeConstraint> constraints,
      List<TypeVarInheritance> inheritance,
      Multimap<String, TVar> namedTypeDefs,
      Multimap<String, TVar> namedTypeRefs) {
    var result = new TypeContext();

    result.context.putAll(context);
    result.unproven.addAll(types);
    constraints.forEach(c -> result.constraints.put(c.typeV(), c));
    inheritance.forEach(
        i -> {
          result.superTypeVs.put(i.subTypeV(), i);
          result.subTypeVs.put(i.superTypeV(), i);
        });
    result.namedTypeDefs.putAll(namedTypeDefs);
    result.namedTypeRefs.putAll(namedTypeRefs);

    return result;
  }

  public void inject(Stdlib lib) {
    // add stdlib
    var stdlib = Stdlib.STD_LIB.TYPE_CONTEXT;
    this.context.putAll(stdlib.context);
    this.unproven.addAll(stdlib.unproven);
    this.constraints.putAll(stdlib.constraints);
    this.superTypeVs.putAll(stdlib.superTypeVs);
    this.subTypeVs.putAll(stdlib.subTypeVs);
  }

  public static TypeContext linkSources(
      ImmutableList<TypeContext> toMerge, Multimap<URI, Diagnostic> errorSink) {
    var result = new TypeContext();

    for (var tctx : toMerge) {
      result.namedTypeDefs.putAll(tctx.namedTypeDefs);
      result.namedTypeRefs.putAll(tctx.namedTypeRefs);
    }

    checkNamedTypeDefs(result, errorSink);

    for (var tctx : toMerge) {
      result.context.putAll(tctx.context);
      result.constraints.putAll(tctx.constraints);
      result.superTypeVs.putAll(tctx.superTypeVs);
      result.subTypeVs.putAll(tctx.subTypeVs);

      // the only duplicates possible here since type def check
      // are stdlib types, where we don't care if we override values.
      result.unproven.addAll(tctx.unproven);
      result.resolved.putAll(tctx.resolved);
    }

    result.linkTypeRefs(errorSink);
    result.compile(errorSink);
    return result;
  }

  private static boolean checkNamedTypeDefs(TypeContext tctx, Multimap<URI, Diagnostic> errorSink) {
    var error = false;
    for (var typeN : tctx.namedTypeDefs.keySet()) {
      var typeDefs = tctx.namedTypeDefs.get(typeN);
      if (typeDefs.size() > 1) {
        error |= true;
        var related =
            typeDefs.stream()
                .<DiagnosticRelatedInformation>map(
                    typeV -> {
                      return new DiagnosticRelatedInformation(
                          typeV.at().toLocation(), "Also defined here.");
                    })
                .collect(Collectors.toCollection(ArrayList::new));

        // duplicate type definition :(
        for (var typeV : typeDefs) {
          var diagnostic =
              new Diagnostic(
                  typeV.at().range(),
                  String.format("%s defined twice", typeN),
                  DiagnosticSeverity.Error,
                  SOURCE);
          diagnostic.setRelatedInformation(related);
          errorSink.put(typeV.at().document(), diagnostic);
        }
      }
    }
    return error;
  }

  private boolean linkTypeRefs(Multimap<URI, Diagnostic> errorSink) {
    var error = false;
    for (var typeN : namedTypeRefs.keySet()) {
      var typeDefs = namedTypeDefs.get(typeN);
      var typeRefs = namedTypeRefs.get(typeN);

      if (typeDefs.size() == 0) {
        error |= true; // error
        for (var typeRefV : typeRefs) {
          var diagnostic =
              new Diagnostic(
                  typeRefV.at().range(),
                  String.format("Referencing unknown type '%s'", typeN),
                  DiagnosticSeverity.Error,
                  SOURCE);
          errorSink.put(typeRefV.at().document(), diagnostic);
        }
      } else if (typeDefs.size() == 1) {
        // link types only if only one typedef exists
        var typeDefV = typeDefs.get(0);
        for (var typeRefV : typeRefs) {
          // this should schedule necessary type proving
          var inheritance = new TypeVarInheritance(typeDefV, typeRefV);
          error |= constructTypeVInheritance(inheritance, errorSink);
        }
      } else {
        // error already created above
      }
    }
    return error;
  }

  private final ListMultimap<String, TVar> namedTypeDefs =
      MultimapBuilder.hashKeys().arrayListValues().build();
  private final ListMultimap<String, TVar> namedTypeRefs =
      MultimapBuilder.hashKeys().arrayListValues().build();

  private final Map<ParserRuleContext, TVar> context = new HashMap<>();
  private final Map<TVar, ResolvedType> resolved = new HashMap<>();
  private final Map<TVar, TVar> substitutions = new HashMap<>();

  private final ListMultimap<TVar, TypeConstraint> constraints =
      MultimapBuilder.hashKeys().linkedListValues().build();
  private final ListMultimap<TVar, TypeVarInheritance> superTypeVs =
      MultimapBuilder.hashKeys().arrayListValues().build();
  private final ListMultimap<TVar, TypeVarInheritance> subTypeVs =
      MultimapBuilder.hashKeys().arrayListValues().build();

  private final Queue<ResolvedType> unproven = new LinkedBlockingDeque<>();

  // return true if errors were found
  public boolean compile(Multimap<URI, Diagnostic> errorSink) {
    boolean error = false;

    while (!unproven.isEmpty()) {
      var typeD = unproven.poll();
      // substitute type with known substitutions
      typeD = typeD.substitute(substitutions).map(Supplier::get).orElse(typeD);

      // check if there is a conflict with a potentially already resolved type.
      if (resolved.containsKey(typeD.typeV())) {
        var alreadyResolved = resolved.get(typeD.typeV());
        if (!alreadyResolved.type().equalsType(typeD.type())) {
          error |= true;
          String msg =
              String.format(
                  "Resolved to be of type %s, but expected to be of type %s",
                  printType(alreadyResolved.type()), printType(typeD.type()));
          var diagnostic =
              new Diagnostic(
                  alreadyResolved.location().range(), msg, DiagnosticSeverity.Error, SOURCE);
          errorSink.put(alreadyResolved.location().document(), diagnostic);
          error |= true;
        }
      }
      // check if constraints are fulfilled
      else { // no need if type was already propagated downwards
        error |= checkSuperTypeConstraints(typeD, typeD.typeV(), errorSink);
        // continue for now :)

        if (!error) {
          // set type as ground truth
          resolved.put(typeD.typeV(), typeD);
          for (var inheritance : subTypeVs.get(typeD.typeV())) {
            var inferredType =
                new TypeInferred(inheritance.subTypeV(), typeD.type(), typeD.location());
            unproven.add(inferredType);
          }
        }
      }
    }
    return error;
  }

  private boolean checkSuperTypeConstraints(
      ResolvedType typeR, TVar typVar, Multimap<URI, Diagnostic> errorSink) {
    var error = false;
    if (resolved.containsKey(typVar)) {
      // we skip constraint checking if we encounter a type variable
      // which was not fully propagated downwards yet
      // aka to the outmost expression in the parse tree
      // a constraint produced by this to be checked type will yield better errors messages
      // and we avoid duplicate constraint checking
      // if types are not compatible we'll get and error at the propagation stage
      return error;
    }
    for (var constraint : constraints.get(typVar)) {
      error |= checkFulfillsConstraint(typeR, constraint, errorSink);
    }
    for (var superTypeV : superTypeVs.get(typVar)) {
      error |= checkSuperTypeConstraints(typeR, superTypeV.superTypeV(), errorSink);
    }
    return error;
  }

  private boolean checkFulfillsConstraint(
      ResolvedType typeR, TypeConstraint constraint, Multimap<URI, Diagnostic> errorSink) {
    var error = false;
    var type = typeR.type();
    var typeV = typeR.typeV();
    var cType = constraint.type();
    var cTypeV = constraint.typeV();

    if (cType instanceof H3xOverloadable overloadable) {
      if (!type.supports(overloadable)) {
        error |= true;
        var msg =
            String.format(
                "%s: %s is not applicable from %s",
                String.format(cTypeV.format(), cTypeV.args()),
                printType(constraint.type()),
                printType(typeR.type()));
        var diagnostic = new Diagnostic(typeV.at().range(), msg, DiagnosticSeverity.Error, SOURCE);
        errorSink.put(typeV.at().document(), diagnostic);
      }
    } else if (!cType.getClass().isInstance(type)) {
      error = true;
      var msg =
          String.format(
              "%s: %s is not applicable from %s",
              String.format(cTypeV.format(), cTypeV.args()),
              printType(constraint.type()),
              printType(typeR.type()));

      var diagnostic = new Diagnostic(typeV.at().range(), msg, DiagnosticSeverity.Error, SOURCE);
      diagnostic.setRelatedInformation(
          Lists.newArrayList(
              new DiagnosticRelatedInformation(
                  typeR.location().toLocation(),
                  String.format("Definition: %s", printType(typeR.typeV())))));
      errorSink.put(typeV.at().document(), diagnostic);
    }

    if (type instanceof H3xMap mType && cType instanceof H3xMap cMType) {
      var mTypeMap = mType.entries();
      // theres is expected to be only one entry, but we iterate anyway
      for (var cMTypeEntry : cMType.entries().entrySet()) {
        var cMKey = cMTypeEntry.getKey();
        var cMValueTypeV = cMTypeEntry.getValue();
        if (mTypeMap.containsKey(cMKey)) {
          var mValueTypeV = mTypeMap.get(cMKey);
          var inheritance = new TypeVarInheritance(mValueTypeV, cMValueTypeV);
          // construct tvar inheritance
          error |= constructTypeVInheritance(inheritance, errorSink);
        } else {
          error |= true;
          String msg = String.format("Unknown member '%s'", cMKey);
          var diagnostic =
              new Diagnostic(cTypeV.at().range(), msg, DiagnosticSeverity.Error, SOURCE);
          var relDiagnostic =
              new DiagnosticRelatedInformation(
                  typeR.location().toLocation(),
                  String.format("Definition: %s", printType(typeR.typeV())));
          diagnostic.setRelatedInformation(Lists.newArrayList(relDiagnostic));
          errorSink.put(cTypeV.at().document(), diagnostic);
        }
      }
    } else if (type instanceof H3xList lType && cType instanceof H3xList cLType) {
      var lGenTypeV = lType.itemTVar();
      var cLGenTypeV = cLType.itemTVar();
      var inheritance = new TypeVarInheritance(lGenTypeV, cLGenTypeV);
      constructTypeVInheritance(inheritance, errorSink);
    } else if (type instanceof H3xFunction fType && cType instanceof H3xFunction cFType) {
      if (fType.arity() != cFType.arity()) {
        var msg =
            String.format(
                "Function has %s parameters, but %s were given.", fType.arity(), cFType.arity());
        var diagnostic = new Diagnostic(cTypeV.at().range(), msg, DiagnosticSeverity.Error, SOURCE);
        diagnostic.setRelatedInformation(
            Lists.newArrayList(
                new DiagnosticRelatedInformation(
                    typeR.location().toLocation(),
                    String.format("Definition: %s", printType(typeR.type())))));
        errorSink.put(cTypeV.at().document(), diagnostic);
        error |= true;
      } else {
        // construct type var inheritance
        for (int i = -1; i < fType.arity(); i++) {
          var fParamV = i == -1 ? fType.resultType() : fType.argsTypes().get(i);
          var cFParamV = i == -1 ? cFType.resultType() : cFType.argsTypes().get(i);
          var inheritance = new TypeVarInheritance(cFParamV, fParamV);
          error |= constructTypeVInheritance(inheritance, errorSink);
        }

        // this creates safe unifiable tvars
        var unifiableTypeVars = fType.necessaryUnification(cFType);
        var newSubstitution = new HashMap<TVar, TVar>();
        for (var equalityGroup : unifiableTypeVars) {
          TVar sharedTypeV = TVar.create(cTypeV.at(), "Inferred all binding applicaiton.");
          for (var p : equalityGroup) {
            var pTypeV = p == -1 ? cFType.resultType() : cFType.argsTypes().get(p);
            newSubstitution.put(pTypeV, sharedTypeV);
            var inheritance = new TypeVarInheritance(sharedTypeV, pTypeV);
            error |= constructTypeVInheritance(inheritance, errorSink);
          }
        }

        if (!newSubstitution.isEmpty()) {
          // apply join substitutions
          checkState(
              newSubstitution.keySet().stream().noneMatch(substitutions::containsKey),
              "Invalid substitution generated. Duplicate key.");
          substitutions.replaceAll((k, v) -> newSubstitution.getOrDefault(v, v));
          substitutions.putAll(newSubstitution);

          var provenIter = resolved.entrySet().iterator();
          while (provenIter.hasNext()) {
            var proven = provenIter.next();
            if (proven.getValue() instanceof TypeDef def) {
              if (def.substitute(substitutions).isPresent()) {
                // if substitutable then reschedule and invalidate proof
                unproven.add(def);
                provenIter.remove();
              }
            } else if (proven.getValue() instanceof TypeInferred inf) {
              if (inf.substitute(substitutions).isPresent()) {
                // if substitutable then invalidate proof
                provenIter.remove();
              }
            }
          }

          for (var k : constraints.keySet()) {
            var vIter = constraints.get(k).listIterator();
            while (vIter.hasNext()) {
              var c = vIter.next();
              c.substitute(substitutions)
                  .ifPresent(
                      newConstraint -> {
                        vIter.set(newConstraint.get());
                      });
            }
          }
        }
      }
    }
    return error;
  }

  private boolean superConstraintExistsInHierarchy(TVar subTypeV, TVar superTypeV) {
    if (subTypeV.equals(superTypeV)) {
      return true;
    }
    var explored = new HashSet<TVar>();
    var queued = new ConcurrentLinkedQueue<TVar>();
    queued.add(subTypeV);
    while (!queued.isEmpty()) {
      var subV = queued.poll();
      for (var inh : superTypeVs.get(subV)) {
        var superV = inh.superTypeV();
        if (superTypeV.equals(superV)) {
          return true;
        }
        // only explore if not already explored
        if (explored.add(superV)) {
          queued.add(superV);
        }
      }
    }
    return false;
  }

  private boolean constructTypeVInheritance(
      TypeVarInheritance inheritance, Multimap<URI, Diagnostic> errorSink) {
    var error = false;
    var subTypeV = inheritance.subTypeV();
    var superTypeV = inheritance.superTypeV();
    var inferredSubType = resolved.containsKey(subTypeV);
    var inferredSuperType = resolved.containsKey(superTypeV);

    // skip construction if already exists
    if (superConstraintExistsInHierarchy(inheritance.subTypeV(), inheritance.superTypeV())) {
      return false;
    }

    if (inferredSuperType) {
      // schedule down propagation!
      var superType = resolved.get(superTypeV);
      var newType = new TypeInferred(subTypeV, superType.type(), subTypeV.at());
      unproven.add(newType);
    } else if (inferredSubType) {
      var subType = resolved.get(subTypeV);
      // we skip subT, since we assume it was already proven
      error |= checkSuperTypeConstraints(subType, superTypeV, errorSink);
    } else {
      Queue<TVar> discoverSubVarQueue = new LinkedBlockingDeque<>();
      discoverSubVarQueue.add(subTypeV);

      while (!discoverSubVarQueue.isEmpty()) {
        var typeV = discoverSubVarQueue.poll();
        if (resolved.containsKey(typeV)) {
          // either queue constraint check or immediately check constraints
          // check only constraints from freshly discovered constraint
          error |= checkSuperTypeConstraints(resolved.get(typeV), superTypeV, errorSink);
        } else {
          // add all subtype vars
          for (var v : subTypeVs.get(typeV)) {
            discoverSubVarQueue.add(v.subTypeV());
          }
        }
      }
    }
    // don't establish constraint on error
    if (!error) {
      superTypeVs.put(inheritance.subTypeV(), inheritance);
      subTypeVs.put(inheritance.superTypeV(), inheritance);
    }
    return error;
  }

  private String printType(TVar typeV) {
    var typeD = resolved.get(typeV);
    if (typeD == null) {
      return String.format("%s:?", typeV);
    }
    return printType(typeD.type());
  }

  private String printType(H3xType type) {
    if (type instanceof H3xMap mapT) {
      var joiner = Joiner.on(", ").withKeyValueSeparator(":");
      var entries =
          mapT.entries().entrySet().stream()
              .collect(
                  ImmutableMap.toImmutableMap(Entry::getKey, e -> this.printType(e.getValue())));
      if (mapT instanceof H3xDocument doc) {
        return String.format("'%s'", doc.name(), joiner.join(entries));
      } else {
        return String.format("Map{%s}", joiner.join(entries));
      }
    } else if (type instanceof H3xList listT) {
      return String.format("List[%s]", this.printType(listT.itemTVar()));
    } else if (type instanceof H3xFunction fktT) {
      return String.format(
          "FKT<%s>",
          Stream.concat(fktT.argsTypes().stream(), Stream.of(fktT.resultType()))
              .map(this::printType)
              .collect(Collectors.joining(", ")));
    }
    return String.format("%s", type);
  }
}
