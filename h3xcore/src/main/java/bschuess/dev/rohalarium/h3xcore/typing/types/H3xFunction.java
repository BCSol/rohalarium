package bschuess.dev.rohalarium.h3xcore.typing.types;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.MultimapBuilder;
import com.google.common.collect.Streams;

import bschuess.dev.rohalarium.h3xcore.typing.TVar;

public record H3xFunction(TVar resultType, ImmutableList<TVar> argsTypes) implements H3xType {

  public H3xFunction {
    checkNotNull(resultType);
    checkNotNull(argsTypes);
    checkArgument(!argsTypes.isEmpty(), "Functino with 0 arguments is not permitted");
  }

  public int arity() {
    return argsTypes.size();
  }

  @Override
  public boolean equalsType(H3xType type) {
    if (type instanceof H3xFunction other) {
      return this.resultType.equals(other.resultType)
          && this.argsTypes.size() == other.argsTypes.size()
          && Streams.zip(this.argsTypes.stream(), other.argsTypes.stream(), Objects::equals)
              .allMatch(Boolean::booleanValue);
    }
    return false;
  }

  @Override
  public String toString() {
    return String.format(
        "FKT<%s>",
        Stream.concat(argsTypes.stream(), Stream.of(resultType))
            .map(Object::toString)
            .collect(Collectors.joining(",")));
  }

  @Override
  public Optional<Supplier<H3xType>> substitute(Map<TVar, TVar> substitution) {
    var doSubstitute =
        substitution.containsKey(resultType)
            || argsTypes.stream().anyMatch(substitution::containsKey);
    if (doSubstitute) {
      return Optional.of(
          () -> {
            var resultV = substitution.getOrDefault(resultType, resultType);
            var argVs =
                argsTypes.stream()
                    .map(e -> substitution.getOrDefault(e, e))
                    .collect(ImmutableList.toImmutableList());
            return new H3xFunction(resultV, argVs);
          });
    }
    return Optional.empty();
  }

  /** Returns which type variables need to be grouped in target function. */
  public Collection<ImmutableList<Integer>> necessaryUnification(H3xFunction target) {
    var groupGraph =
        MultimapBuilder.hashKeys().arrayListValues().<Integer, ImmutableList<Integer>>build();
    var targetGroups = target.typeVEqClasses();

    Streams.concat(typeVEqClasses().stream(), targetGroups.stream())
        .forEach(
            g -> {
              for (var index : g) {
                groupGraph.put(index, g);
              }
            });
    var exploredIndexes = new HashSet<>();
    var queue = new ConcurrentLinkedQueue<Integer>();
    var result = new ArrayList<ImmutableList<Integer>>();
    for (var index = -1; index < arity(); index++) {
      if (!exploredIndexes.contains(index)) {
        var group = new HashSet<Integer>();
        queue.add(index);
        while (!queue.isEmpty()) {
          var i = queue.poll();
          exploredIndexes.add(i);
          group.add(i);
          groupGraph.get(i).stream()
              .flatMap(ImmutableList::stream)
              .distinct()
              .filter(Predicates.not(exploredIndexes::contains))
              .forEach(queue::add);
        }
        // if no information through application can be gained, don't unify
        if (group.size() > 1 && targetGroups.stream().noneMatch(g -> g.containsAll(group))) {
          result.add(ImmutableList.copyOf(group));
        }
      }
    }
    return result;
  }

  private Collection<ImmutableList<Integer>> typeVEqClasses() {
    var eqClasses =
        IntStream.range(-1, argsTypes.size())
            .boxed()
            .collect(
                Collectors.groupingBy(
                    i -> i == -1 ? resultType : argsTypes.get(i),
                    HashMap::new,
                    ImmutableList.toImmutableList()));
    // we are only interested in type vars with multiple occurrences
    eqClasses.entrySet().removeIf(e -> e.getValue().size() <= 1);
    return eqClasses.values();
  }
}
