package bschuess.dev.rohalarium.h3xcore.typing;

import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

import bschuess.dev.rohalarium.h3xcore.typing.types.H3xType;

public interface ResolvedType {

  public TVar typeV();

  public H3xType type();

  public Locator location();

  /**
   * Returns an optional of a supplier yielding the result of a substitution.
   *
   * <p>The optional is present, if the type contains type variables that may be substituted. We
   * return a supplier to avoid unnecessary object creation.
   */
  public Optional<Supplier<ResolvedType>> substitute(Map<TVar, TVar> substitution);
}
