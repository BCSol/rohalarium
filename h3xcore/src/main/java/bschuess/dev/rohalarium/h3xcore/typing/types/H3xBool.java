package bschuess.dev.rohalarium.h3xcore.typing.types;

public class H3xBool implements H3xPrimitive {
  public static final H3xBool BOOL = new H3xBool();

  private H3xBool() {}

  @Override
  public boolean supports(H3xOverloadable overloadable) {
    return false;
  }

  @Override
  public java.lang.String toString() {
    return getClass().getSimpleName();
  }
}
