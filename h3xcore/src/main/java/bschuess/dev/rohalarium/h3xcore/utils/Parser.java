package bschuess.dev.rohalarium.h3xcore.utils;

import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.KeyContext;

public class Parser {

  public static String parseKey(KeyContext ctx) {
    var text = ctx.getText();
    if (text.startsWith("'")) {
      text = text.substring(1, text.length() - 1);
    }
    return text;
  }
}
