package bschuess.dev.rohalarium.h3xcore.typing.types;

public interface H3xPrimitive extends H3xType {

  @Override
  public default boolean equalsType(H3xType type) {
    return this == type;
  }
}
