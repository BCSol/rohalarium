package bschuess.dev.rohalarium.h3xcore.parser;

import static bschuess.dev.rohalarium.h3xcore.utils.Parser.parseKey;
import static com.google.common.base.Preconditions.checkNotNull;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.ApplicationContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.BoolContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.DocumentContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.InlineBinaryContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.LambdaContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.ListEntryContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.MapAccessContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.MapEntryContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.NumberContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.ParenthesisContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.RefContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.SelfContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.StringContext;
import bschuess.dev.rohalarium.h3xcore.parser.H3xcoreParser.VariableContext;
import bschuess.dev.rohalarium.h3xcore.typing.Locator;
import bschuess.dev.rohalarium.h3xcore.typing.Stdlib;
import bschuess.dev.rohalarium.h3xcore.typing.TVar;
import bschuess.dev.rohalarium.h3xcore.typing.TypeContext;
import bschuess.dev.rohalarium.h3xcore.typing.types.H3xBool;
import bschuess.dev.rohalarium.h3xcore.typing.types.H3xDocument;
import bschuess.dev.rohalarium.h3xcore.typing.types.H3xFunction;
import bschuess.dev.rohalarium.h3xcore.typing.types.H3xList;
import bschuess.dev.rohalarium.h3xcore.typing.types.H3xMap;
import bschuess.dev.rohalarium.h3xcore.typing.types.H3xNumber;
import bschuess.dev.rohalarium.h3xcore.typing.types.H3xString;
import bschuess.dev.rohalarium.h3xcore.utils.Parser;

public class TypeCollectorVisitor extends H3xcoreParserBaseVisitor<TVar> {

  private final URI document;
  private final TypeCollector tctx;

  private String docName = null;
  private TVar self = null;

  private ImmutableMap<String, TVar> varScope = ImmutableMap.of();

  public TypeCollectorVisitor(URI document) {
    this.document = checkNotNull(document);
    this.tctx = new TypeCollector();
  }

  public TypeContext result() {
    return tctx.result();
  }

  @Override
  public TVar visitDocument(DocumentContext ctx) {
    // we might want to store the key here
    // also initialize inheritance
    this.docName = parseKey(ctx.key());
    var docT = new H3xDocument(docName);
    this.self = tctx.registerTypeDefinition(document, ctx, docT);
    tctx.registerNamedTypeDef(docName, this.self);
    ctx.mapEntry().forEach(eCtx -> visitMapEntry(docT, eCtx));
    return this.self;
  }

  private void visitMapEntry(H3xMap mapType, MapEntryContext ctx) {
    var key = parseKey(ctx.key());
    if (!ctx.mapEntry().isEmpty()) {
      var submapT = new H3xMap();
      var submapV = tctx.registerTypeDefinition(document, ctx, submapT);
      mapType.put(key, submapV);
      ctx.mapEntry().forEach(eCtx -> visitMapEntry(submapT, eCtx));
    } else if (!ctx.listEntry().isEmpty()) {
      var listGenV = TVar.create(Locator.from(document, ctx.key()), "Generic list type.");
      var listT = new H3xList(listGenV);
      var listV = tctx.registerTypeDefinition(document, ctx, listT);
      mapType.put(key, listV);
      ctx.listEntry().forEach(iCtx -> visitListItem(listT, iCtx));
    } else {
      var valueT = visit(ctx.expr());
      mapType.put(key, valueT);
    }
  }

  private void visitListItem(H3xList listT, ListEntryContext ctx) {
    if (!ctx.mapEntry().isEmpty()) {
      var submapT = new H3xMap();
      var submapV = tctx.registerTypeDefinition(document, ctx, submapT);
      tctx.registerTypeVarInheritance(listT.itemTVar(), submapV);
      ctx.mapEntry().forEach(eCtx -> visitMapEntry(submapT, eCtx));
    } else if (!ctx.listEntry().isEmpty()) {
      var sublistGenV = TVar.create(Locator.from(document, ctx), "Generic list type.");
      var sublistT = new H3xList(sublistGenV);
      var sublistV = tctx.registerTypeDefinition(document, ctx, sublistT);
      tctx.registerTypeVarInheritance(listT.itemTVar(), sublistV);
      ctx.listEntry().forEach(iCtx -> visitListItem(sublistT, iCtx));
    } else {
      var valueT = visit(ctx.expr());
      tctx.registerTypeVarInheritance(listT.itemTVar(), valueT);
    }
  }

  @Override
  public TVar visitLambda(LambdaContext ctx) {
    var oldScope = this.varScope;
    var newScope = new HashMap<String, TVar>();
    newScope.putAll(oldScope);

    var paramVs = new ArrayList<TVar>();
    for (var i = 0; i < ctx.params.size(); i++) {
      TVar paramV = TVar.create(Locator.from(document, ctx.params.get(i)), "p%s", i);
      // shadowing var is checked in Syntax Checker
      newScope.put(ctx.params.get(i).getText(), paramV);
      paramVs.add(paramV);
    }
    // parse expression with extended var scope
    this.varScope = ImmutableMap.copyOf(newScope);
    var resultV = this.visit(ctx.expr());
    // reset scope
    this.varScope = oldScope;

    var fktV =
        TVar.create(
            Locator.from(document, ctx),
            "FKT<%s, %s>",
            paramVs.stream().map(TVar::toString).collect(Collectors.joining(", ")),
            resultV);

    var fktT = new H3xFunction(resultV, ImmutableList.copyOf(paramVs));
    tctx.registerStdLibType(fktV, fktT, Locator.from(document, ctx));
    return fktV;
  }

  @Override
  public TVar visitVariable(VariableContext ctx) {
    // no unbound variable check necessary, already done in syntax checker
    return varScope.get(ctx.STR_PLAIN().getText());
  }

  @Override
  public TVar visitInlineBinary(InlineBinaryContext ctx) {
    var opV = Stdlib.STD_LIB.STD_OPS.get(ctx.op.getText());
    if (opV == null) {
      throw new UnsupportedOperationException();
    }
    // operator application
    var applicationResultV =
        TVar.create(Locator.from(document, ctx), "%s: result", ctx.op.getText());
    var applicationArgVs =
        ctx.args.stream().map(this::visit).collect(ImmutableList.toImmutableList());

    tctx.registerApplicationConstraint(opV, applicationResultV, applicationArgVs);
    return applicationResultV;
  }

  @Override
  public TVar visitParenthesis(ParenthesisContext ctx) {
    return visit(ctx.expr());
  }

  @Override
  public TVar visitSelf(SelfContext ctx) {
    assert this.self != null;
    TVar selfRef =
        TVar.create(Locator.from(document, ctx.SELF().getSymbol()), "'%s'", this.docName);
    tctx.registerTypeVarInheritance(this.self, selfRef);
    return selfRef;
  }

  @Override
  public TVar visitRef(RefContext ctx) {
    var otherName = Parser.parseKey(ctx.key());
    TVar otherRef = TVar.create(Locator.from(document, ctx), "'%s'", otherName);
    tctx.registerNamedTypeRef(otherName, otherRef);
    return otherRef;
  }

  @Override
  public TVar visitMapAccess(MapAccessContext ctx) {
    String key = parseKey(ctx.key());
    var valueV = TVar.create(Locator.from(document, ctx), ".'%s'", key);
    var mapT = new H3xMap();
    mapT.put(key, valueV);
    TVar expr = visit(ctx.expr());
    tctx.registerTypeConstraint(expr, mapT);
    return valueV;
  }

  @Override
  public TVar visitApplication(ApplicationContext ctx) {
    var fktV = visit(ctx.fkt);
    var argVs = ctx.args.stream().map(this::visit).collect(ImmutableList.toImmutableList());
    var resultV = TVar.create(Locator.from(document, ctx), "Application");
    tctx.registerApplicationConstraint(fktV, resultV, argVs);
    return resultV;
  }

  @Override
  public TVar visitBool(BoolContext ctx) {
    return tctx.registerTypeDefinition(document, ctx, H3xBool.BOOL);
  }

  @Override
  public TVar visitNumber(NumberContext ctx) {
    return tctx.registerTypeDefinition(document, ctx, H3xNumber.NUMBER);
  }

  @Override
  public TVar visitString(StringContext ctx) {
    return tctx.registerTypeDefinition(document, ctx, H3xString.STRING);
  }
}
