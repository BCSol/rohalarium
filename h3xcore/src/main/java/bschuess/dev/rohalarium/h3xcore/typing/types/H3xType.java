package bschuess.dev.rohalarium.h3xcore.typing.types;

import java.util.Optional;
import java.util.function.Supplier;

import bschuess.dev.rohalarium.h3xcore.typing.TVar;

public interface H3xType {

  public default boolean supports(H3xOverloadable overloadable) {
    return false;
  }

  public default Optional<Supplier<H3xType>> substitute(java.util.Map<TVar, TVar> substitution) {
    return Optional.empty();
  }

  public boolean equalsType(H3xType type);
}
