# Rohalarium

The goal of `Rohalarium` is to enable `Pen'n'Paper` enthusiasts to write down their fanatisy worlds and rule sets in a declarative, human readable style.
And provide a set of tools for their imagined worlds:
  - `World` creation tool
  -  Automatic `Wiki` generation 
  - `Hero` creation tools

## H3xcore Script

At it's core lies a yaml based dataformat with scripting capabilties.

