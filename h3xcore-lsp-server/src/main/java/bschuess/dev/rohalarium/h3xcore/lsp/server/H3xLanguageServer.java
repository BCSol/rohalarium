package bschuess.dev.rohalarium.h3xcore.lsp.server;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.concurrent.CompletableFuture;

import org.eclipse.lsp4j.DocumentFormattingOptions;
import org.eclipse.lsp4j.DocumentSymbolOptions;
import org.eclipse.lsp4j.FileOperationsServerCapabilities;
import org.eclipse.lsp4j.InitializeParams;
import org.eclipse.lsp4j.InitializeResult;
import org.eclipse.lsp4j.SemanticTokensServerFull;
import org.eclipse.lsp4j.SemanticTokensWithRegistrationOptions;
import org.eclipse.lsp4j.ServerCapabilities;
import org.eclipse.lsp4j.TextDocumentSyncKind;
import org.eclipse.lsp4j.TextDocumentSyncOptions;
import org.eclipse.lsp4j.WorkspaceFoldersOptions;
import org.eclipse.lsp4j.WorkspaceServerCapabilities;
import org.eclipse.lsp4j.services.LanguageClient;
import org.eclipse.lsp4j.services.LanguageClientAware;
import org.eclipse.lsp4j.services.LanguageServer;
import org.eclipse.lsp4j.services.TextDocumentService;
import org.eclipse.lsp4j.services.WorkspaceService;

import com.google.inject.Inject;

import bschuess.dev.rohalarium.h3xcore.parser.SemanticTokens;

public class H3xLanguageServer implements LanguageServer, LanguageClientAware {

  private final H3xWorkspaceCompiler compiler;

  private LanguageClient client;

  @Inject
  public H3xLanguageServer(H3xWorkspaceCompiler compiler) {
    this.compiler = checkNotNull(compiler);
  }

  @Override
  public void connect(LanguageClient client) {
    client = checkNotNull(client);
  }

  @Override
  public CompletableFuture<InitializeResult> initialize(InitializeParams params) {
    var result = new InitializeResult(createServerCapabilities()); // TODO server info?
    compiler.onWorkspaceOpened(params);
    return CompletableFuture.completedFuture(result);
  }

  private ServerCapabilities createServerCapabilities() {
    var caps = new ServerCapabilities();

    // formatting capability
    var formatOpts = new DocumentFormattingOptions();
    formatOpts.setWorkDoneProgress(false);
    caps.setDocumentFormattingProvider(formatOpts);

    // file synchronization
    var syncOpts = new TextDocumentSyncOptions();
    syncOpts.setOpenClose(true);
    syncOpts.setChange(TextDocumentSyncKind.Full);
    caps.setTextDocumentSync(syncOpts);

    var docSymbolOpts = new DocumentSymbolOptions();
    caps.setDocumentSymbolProvider(docSymbolOpts);

    // sematic tokens
    var sematicTokenOpts =
        new SemanticTokensWithRegistrationOptions(
            SemanticTokens.legend(), new SemanticTokensServerFull(false), false);
    caps.setSemanticTokensProvider(sematicTokenOpts);

    // workspace
    var workspaceCaps = new WorkspaceServerCapabilities();
    caps.setWorkspace(workspaceCaps);

    var fileOperationsCaps = new FileOperationsServerCapabilities();
    workspaceCaps.setFileOperations(fileOperationsCaps);

    var workspaceFolderOpts = new WorkspaceFoldersOptions();
    workspaceFolderOpts.setSupported(true);
    workspaceFolderOpts.setChangeNotifications(true);
    workspaceCaps.setWorkspaceFolders(workspaceFolderOpts);

    return caps;
  }

  @Override
  public CompletableFuture<Object> shutdown() {
    // FIXME
    return CompletableFuture.completedFuture(null);
  }

  @Override
  public void exit() {
    System.exit(0);
  }

  @Override
  public TextDocumentService getTextDocumentService() {
    return compiler;
  }

  @Override
  public WorkspaceService getWorkspaceService() {
    return compiler;
  }
}
