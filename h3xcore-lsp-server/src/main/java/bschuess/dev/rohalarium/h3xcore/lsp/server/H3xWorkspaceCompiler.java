package bschuess.dev.rohalarium.h3xcore.lsp.server;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.not;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.DidChangeConfigurationParams;
import org.eclipse.lsp4j.DidChangeTextDocumentParams;
import org.eclipse.lsp4j.DidChangeWatchedFilesParams;
import org.eclipse.lsp4j.DidChangeWorkspaceFoldersParams;
import org.eclipse.lsp4j.DidCloseTextDocumentParams;
import org.eclipse.lsp4j.DidOpenTextDocumentParams;
import org.eclipse.lsp4j.DidSaveTextDocumentParams;
import org.eclipse.lsp4j.DocumentFormattingParams;
import org.eclipse.lsp4j.DocumentSymbol;
import org.eclipse.lsp4j.DocumentSymbolParams;
import org.eclipse.lsp4j.FileChangeType;
import org.eclipse.lsp4j.FileEvent;
import org.eclipse.lsp4j.InitializeParams;
import org.eclipse.lsp4j.MessageParams;
import org.eclipse.lsp4j.MessageType;
import org.eclipse.lsp4j.Position;
import org.eclipse.lsp4j.PublishDiagnosticsParams;
import org.eclipse.lsp4j.Range;
import org.eclipse.lsp4j.SemanticTokens;
import org.eclipse.lsp4j.SemanticTokensParams;
import org.eclipse.lsp4j.SymbolInformation;
import org.eclipse.lsp4j.TextEdit;
import org.eclipse.lsp4j.WorkspaceFolder;
import org.eclipse.lsp4j.jsonrpc.messages.Either;
import org.eclipse.lsp4j.services.LanguageClient;
import org.eclipse.lsp4j.services.LanguageClientAware;
import org.eclipse.lsp4j.services.TextDocumentService;
import org.eclipse.lsp4j.services.WorkspaceService;

import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.MultimapBuilder;

import bschuess.dev.rohalarium.h3xcore.compiler.CompiledDocument;
import bschuess.dev.rohalarium.h3xcore.compiler.CompiledWorkspace;
import bschuess.dev.rohalarium.h3xcore.compiler.DocumentCompiler;
import bschuess.dev.rohalarium.h3xcore.parser.DocumentSymbolVisitor;
import bschuess.dev.rohalarium.h3xcore.parser.FormattingASTVisitor;
import bschuess.dev.rohalarium.h3xcore.parser.SemanticTokenVisitor;
import bschuess.dev.rohalarium.h3xcore.typing.Stdlib;
import bschuess.dev.rohalarium.h3xcore.typing.TypeContext;
import edu.umd.cs.findbugs.annotations.Nullable;

public class H3xWorkspaceCompiler
    implements WorkspaceService, TextDocumentService, LanguageClientAware {

  private LanguageClient client;

  @Override
  public void connect(LanguageClient client) {
    this.client = checkNotNull(client);
  }

  private final Map<URI, String> openedFiles = new HashMap<>();
  private final Map<URI, CompiledDocument> docsOpened = new HashMap<>();
  private final Map<URI, CompiledDocument> docsOnDisk = new HashMap<>();

  private CompiledWorkspace compilationResult =
      new CompiledWorkspace(
          TypeContext.EMPTY, MultimapBuilder.hashKeys().arrayListValues().build());

  @Override
  public CompletableFuture<List<Either<SymbolInformation, DocumentSymbol>>> documentSymbol(
      DocumentSymbolParams params) {

    var uri = URI.create(params.getTextDocument().getUri());
    var doc = docsOpened.get(uri);
    if (doc == null || doc.fileInput().isEmpty()) {
      return CompletableFuture.completedFuture(new ArrayList<>());
    }

    var fileInput = doc.fileInput().orElseThrow();
    var symbols = new ArrayList<Either<SymbolInformation, DocumentSymbol>>();
    var visitor = new DocumentSymbolVisitor();
    for (var document : fileInput.documents) {
      symbols.add(Either.forRight(visitor.visit(document)));
    }
    return CompletableFuture.completedFuture(symbols);
  }

  @Override
  public CompletableFuture<SemanticTokens> semanticTokensFull(SemanticTokensParams params) {

    var tokenData = new ArrayList<Integer>();
    var tokens = new SemanticTokens(tokenData);
    var result = CompletableFuture.completedFuture(tokens);

    var uri = URI.create(params.getTextDocument().getUri());
    var doc = docsOpened.get(uri);
    if (doc == null || doc.fileInput().isEmpty()) {
      return result;
    }

    var semanticTokenizer = new SemanticTokenVisitor();
    semanticTokenizer.visit(doc.fileInput().orElseThrow());
    tokenData.addAll(semanticTokenizer.getTokens());
    return result;
  }

  @Override
  public CompletableFuture<List<? extends TextEdit>> formatting(DocumentFormattingParams params) {
    var uri = URI.create(params.getTextDocument().getUri());
    var doc = docsOpened.get(uri);
    if (doc == null || doc.fileInput().isEmpty()) {
      return CompletableFuture.completedFuture(new ArrayList<>());
    }

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    try (BufferedOutputStream bos = new BufferedOutputStream(baos);
        PrintStream out = new PrintStream(bos, false, Charsets.UTF_8)) {
      var formatter = new FormattingASTVisitor(out);
      formatter.visit(doc.fileInput().orElseThrow());
    } catch (IOException e) {
      return CompletableFuture.completedFuture(new ArrayList<>());
    }

    var formatted = baos.toString();
    var range =
        new Range(
            new Position(0, 0),
            // no +1 needed lsp starts counting at 0
            new Position(doc.fileInput().orElseThrow().stop.getLine(), 0));
    var edit = new TextEdit(range, formatted);
    return CompletableFuture.completedFuture(Lists.newArrayList(edit));
  }

  private void compileWorkspace() {
    ListMultimap<URI, Diagnostic> errors = MultimapBuilder.hashKeys().arrayListValues().build();

    var docs = new HashMap<URI, CompiledDocument>();
    docs.putAll(docsOnDisk);
    docs.putAll(docsOpened); // override compiled documents from opened files

    docs.values().forEach(doc -> errors.putAll(doc.diagnostics()));
    var result =
        TypeContext.linkSources(
            docs.values().stream()
                .map(CompiledDocument::tctx)
                .filter(not(TypeContext.EMPTY::equals))
                .collect(ImmutableList.toImmutableList()),
            errors);
    compilationResult = new CompiledWorkspace(result, errors);

    if (client != null) {
      // if the connection is not properly established yet avoid sending diagnostics
      // report empty errors as well
      for (var uri : Iterables.concat(docs.keySet(), ImmutableList.of(Stdlib.URI))) {
        var errorList = errors.get(uri);
        var pub = new PublishDiagnosticsParams(uri.toString(), errorList);
        client.publishDiagnostics(pub);
      }
    }
  }

  private void compileDocOpened(URI doc) {
    var source = openedFiles.get(doc);
    if (source != null) {
      var compUnit = DocumentCompiler.compile(doc, source);
      docsOpened.put(doc, compUnit);
    }
    compileWorkspace();
  }

  private void compileDocsOnDisk(ImmutableList<URI> docs) {
    for (var doc : docs) {
      CharStream source;
      try {
        source = CharStreams.fromPath(Path.of(doc));
      } catch (IOException e) {
        var msg =
            new MessageParams(
                MessageType.Error, String.format("Could not parse '%s' (%s).", doc, e));
        client.showMessage(msg);
        return;
      }
      var compUnit = DocumentCompiler.compile(doc, source);
      docsOnDisk.put(doc, compUnit);
    }
    compileWorkspace();
  }

  @Override
  public void didOpen(DidOpenTextDocumentParams params) {
    var uri = URI.create(params.getTextDocument().getUri());
    // we are only interested in h3x files so far
    if (params.getTextDocument().getLanguageId().equals("h3x")) {
      openedFiles.put(uri, params.getTextDocument().getText());
      compileDocOpened(uri);
    }
  }

  @Override
  public void didChange(DidChangeTextDocumentParams params) {
    var uri = URI.create(params.getTextDocument().getUri());
    var changes = params.getContentChanges();
    // we chose full synchronization mode // so the last one is the most recent doc
    var lastChange = changes.get(changes.size() - 1);
    var changed = openedFiles.computeIfPresent(uri, (key, content) -> lastChange.getText());
    if (changed != null) {
      compileDocOpened(uri);
    }
  }

  @Override
  public void didClose(DidCloseTextDocumentParams params) {
    var uri = URI.create(params.getTextDocument().getUri());
    openedFiles.remove(uri);
    docsOpened.remove(uri);
    if (docsOnDisk.containsKey(uri)) {
      compileDocsOnDisk(ImmutableList.of(uri));
    }
  }

  @Override
  public void didSave(DidSaveTextDocumentParams params) {
    var uri = URI.create(params.getTextDocument().getUri());
    if (docsOnDisk.containsKey(uri)) {
      compileDocsOnDisk(ImmutableList.of(uri));
    }
  }

  @Override
  public void didChangeConfiguration(DidChangeConfigurationParams params) {
    // nothing to do so far
  }

  private ImmutableList<URI> h3xFilesInFolder(@Nullable List<WorkspaceFolder> folders) {
    if (folders == null) {
      return ImmutableList.of();
    }
    var filesBuilder = ImmutableList.<URI>builder();
    for (var folder : folders) {
      var uri = URI.create(folder.getUri());
      try {

        Files.walk(Path.of(uri))
            .filter(p -> p.toFile().isFile())
            .filter(p -> p.getFileName().toString().endsWith(".h3x"))
            .map(Path::toUri)
            .forEach(filesBuilder::add);
      } catch (IOException e) {
        // FIXME logging
      }
    }
    return filesBuilder.build();
  }

  public void onWorkspaceOpened(InitializeParams params) {
    var files = h3xFilesInFolder(params.getWorkspaceFolders());
    compileDocsOnDisk(files);
  }

  @Override
  public void didChangeWorkspaceFolders(DidChangeWorkspaceFoldersParams params) {
    var changeEvent = params.getEvent();

    for (var removed : changeEvent.getRemoved()) {
      var uri = removed.getUri();
      var iter = docsOnDisk.keySet().iterator();
      while (iter.hasNext()) {
        var next = iter.next().toString();
        if (next.startsWith(uri)) {
          iter.remove();
        }
      }
    }
    var newFiles = h3xFilesInFolder(changeEvent.getAdded());
    compileDocsOnDisk(newFiles);
  }

  @Override
  public void didChangeWatchedFiles(DidChangeWatchedFilesParams params) {
    var recompile =
        params.getChanges().stream()
            .filter(c -> c.getUri().endsWith(".h3x"))
            .filter(
                c -> {
                  if (c.getType().equals(FileChangeType.Deleted)) {
                    var uri = URI.create(c.getUri());
                    docsOnDisk.remove(uri);
                    return false;
                  }
                  return true;
                })
            .map(FileEvent::getUri)
            .map(URI::create)
            .collect(ImmutableList.toImmutableList());
    compileDocsOnDisk(recompile);
  }
}
