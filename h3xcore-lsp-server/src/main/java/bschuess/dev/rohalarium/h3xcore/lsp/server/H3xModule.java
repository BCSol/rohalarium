package bschuess.dev.rohalarium.h3xcore.lsp.server;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.lsp4j.services.LanguageClient;
import org.eclipse.lsp4j.services.LanguageClientAware;
import org.eclipse.lsp4j.services.LanguageServer;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.google.inject.TypeLiteral;
import com.google.inject.matcher.Matchers;
import com.google.inject.spi.InjectionListener;
import com.google.inject.spi.TypeEncounter;
import com.google.inject.spi.TypeListener;

public class H3xModule extends AbstractModule implements TypeListener, LanguageClientAware {

  public static final String URI_LOCK = "uri-striped-lock";

  private LanguageClient client = null;
  private final List<LanguageClientAware> postConstruct = new ArrayList<>();

  @Override
  protected void configure() {
    super.configure();
    bindListener(Matchers.any(), this);
    bind(LanguageServer.class).to(H3xLanguageServer.class).in(Scopes.SINGLETON);
    bind(H3xWorkspaceCompiler.class).in(Scopes.SINGLETON);
  }

  @Override
  public void connect(LanguageClient client) {
    synchronized (this) {
      checkState(this.client == null);
      this.client = checkNotNull(client);
    }
    for (var service : postConstruct) {
      service.connect(client);
    }
  }

  @Override
  public <I> void hear(TypeLiteral<I> type, TypeEncounter<I> encounter) {
    encounter.register(
        new InjectionListener<>() {
          @Override
          public void afterInjection(Object injectee) {
            if (injectee instanceof LanguageClientAware aware) {
              synchronized (H3xModule.this) {
                if (client == null) {
                  postConstruct.add(aware);
                  return;
                }
              }
              aware.connect(client);
            }
          }
        });
  }
}
