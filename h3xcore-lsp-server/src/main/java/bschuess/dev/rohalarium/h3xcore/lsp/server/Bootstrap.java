package bschuess.dev.rohalarium.h3xcore.lsp.server;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.ExecutionException;

import org.eclipse.lsp4j.jsonrpc.Launcher;
import org.eclipse.lsp4j.services.LanguageClient;
import org.eclipse.lsp4j.services.LanguageServer;

import com.google.inject.Guice;

public class Bootstrap {

  public static void main(String[] args) throws InterruptedException, ExecutionException {
    start(System.in, System.out);
  }

  private static void start(InputStream in, OutputStream out)
      throws InterruptedException, ExecutionException {
    var module = new H3xModule();
    var injector = Guice.createInjector(module);
    var server = injector.getInstance(LanguageServer.class);
    var launcher =
        new Launcher.Builder<LanguageClient>()
            .setLocalService(server)
            .setRemoteInterface(LanguageClient.class)
            .setInput(in)
            .setOutput(out)
            .create();
    var client = launcher.getRemoteProxy();
    module.connect(client);

    launcher.startListening().get();
  }
}
