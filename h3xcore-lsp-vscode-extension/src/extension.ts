// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as path from "path";
import * as vscode from "vscode";
import { workspace, ExtensionContext } from "vscode";
import {
  LanguageClient,
  LanguageClientOptions,
  Executable,
} from "vscode-languageclient/node";

let client: LanguageClient;

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
  let config = vscode.workspace.getConfiguration("h3x");
  let java_home = config.get<string | undefined>("h3x.java.home");

  // Use the console to output diagnostic information (console.log) and errors (console.error)
  // This line of code will only be executed once when your extension is activated
  console.log('Congratulations, your extension "h3x" is now active!');
  console.log(context.extensionPath);

  let server_jar = path.resolve(
    context.extensionPath,
    "h3xcore-lsp-server-0.0.0-jar-with-dependencies.jar"
  );
  let server_exec: Executable = {
    command: java_home ? java_home : "/usr/bin/java",
    args: [
      "-Xdebug",
      "-agentlib:jdwp=transport=dt_socket,server=y,address=127.0.0.1:8000,suspend=n,quiet=y",
      "-jar",
      server_jar,
    ],
  };

  const options: LanguageClientOptions = {
    documentSelector: [{ language: "h3x" }],
    synchronize: {
      fileEvents: workspace.createFileSystemWatcher("**/*.h3x"),
    },
  };

  client = new LanguageClient(
    "h3x",
    "h3x language server",
    server_exec,
    options
  );
  let disposable = client.start();
}

// this method is called when your extension is deactivated
export function deactivate() {}
